'use strict';

const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validate = require('express-validation');
const auth = require('../middlewares/authentication');
const fileCtrl = require('../controllers/file');

router.use(auth.required);

router.post('/:shop',
    validate({
        params: {
            shop: Joi.string().required(),
        }
    }),
    fileCtrl.upload
);

module.exports = router;