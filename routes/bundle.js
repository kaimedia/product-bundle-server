'use strict';

const express = require('express');
const router = express.Router();
const auth = require('../middlewares/authentication');
const bundleCtrl = require('../controllers/bundle');
const validateMiddleware = require('../middlewares/validate');

router.use(auth.required);

router.get('/:shop/list', [validateMiddleware.checkShopDomain], validateMiddleware.requiredInstall, bundleCtrl.list);

router.get('/:shop/:id', [validateMiddleware.checkShopDomain, validateMiddleware.checkId], validateMiddleware.requiredInstall, bundleCtrl.get);

router.post('/', [validateMiddleware.checkName], validateMiddleware.requiredInstall, bundleCtrl.create);

router.patch('/:shop/:id', [validateMiddleware.checkShopDomain, validateMiddleware.checkId, validateMiddleware.checkName], validateMiddleware.requiredInstall, bundleCtrl.update);

router.put('/:shop/:id', [validateMiddleware.checkShopDomain, validateMiddleware.checkId, validateMiddleware.checkStatusInterger], validateMiddleware.requiredInstall, bundleCtrl.status);

router.delete('/:shop/:id', [validateMiddleware.checkShopDomain, validateMiddleware.checkId], validateMiddleware.requiredInstall, bundleCtrl.remove);

module.exports = router;