'use strict';

const express = require('express');
const router = express.Router();
const installCtrl = require('../controllers/install');
const validateMiddleware = require('../middlewares/validate');

router.get('/', [validateMiddleware.checkShopDomain], validateMiddleware.requiredInstall, installCtrl.index);
router.get('/callback', [validateMiddleware.checkShopDomain, validateMiddleware.checkHmac], validateMiddleware.requiredInstall, installCtrl.callback);

module.exports = router;