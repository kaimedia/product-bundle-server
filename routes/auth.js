'use strict';

const express = require('express');
const router = express.Router();
const auth = require('../middlewares/authentication');
const authController = require('../controllers/auth');
const validateMiddleware = require('../middlewares/validate');

router.post('/register', [
    validateMiddleware.checkFirstName,
    validateMiddleware.checkLastName,
    validateMiddleware.checkEmail,
    validateMiddleware.checkPassword,
],
    validateMiddleware.requiredInstall,
    authController.register
);

router.post('/login', [validateMiddleware.checkEmail], validateMiddleware.requiredInstall, authController.login);

router.get('/logout', auth.required, authController.logout);

router.get('/me', auth.required, authController.me);

module.exports = router;