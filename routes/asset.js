'use strict';

const express = require('express');
const router = express.Router();
const assetCtrl = require('../controllers/asset');
const validateMiddleware = require('../middlewares/validate');

router.get('/js/product_bundle.js', [validateMiddleware.checkShopDomain, validateMiddleware.checkStyleURI], validateMiddleware.requiredInstall, assetCtrl.productBundleScript);

router.post('/generate_discount_code/:shop', [validateMiddleware.checkShopDomain], validateMiddleware.requiredInstall, assetCtrl.generateDiscountCode);

module.exports = router;