'use strict';

const express = require('express');
const router = express.Router();
const auth = require('../middlewares/authentication');
const userController = require('../controllers/user');
const validateMiddleware = require('../middlewares/validate');

router.use(auth.required);

router.post('/',
    [validateMiddleware.checkFirstName,
    validateMiddleware.checkLastName,
    validateMiddleware.checkEmail,
    validateMiddleware.checkPassword],
    validateMiddleware.requiredInstall,
    userController.create
);

router.get('/', userController.all);

router.get('/:id', [validateMiddleware.checkId], validateMiddleware.requiredInstall, userController.get);

router.delete('/:id', [validateMiddleware.checkId], validateMiddleware.requiredInstall, userController.remove);

router.patch('/', [validateMiddleware.check_Id], validateMiddleware.requiredInstall, userController.update);

router.patch('/password', [validateMiddleware.check_Id, validateMiddleware.checkNewPassword], validateMiddleware.requiredInstall, userController.password);

module.exports = router;