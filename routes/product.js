'use strict';

const express = require('express');
const router = express.Router();
const auth = require('../middlewares/authentication');
const productCtrl = require('../controllers/product');
const validateMiddleware = require('../middlewares/validate');

router.use(auth.required);

router.get('/:shop/:type/list/:add_type', [validateMiddleware.checkShopDomain, validateMiddleware.checkType], validateMiddleware.requiredInstall, productCtrl.list);

module.exports = router;