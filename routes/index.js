'use strict';

const express = require('express');
const router = express.Router();
const validateMiddleware = require('../middlewares/validate');
const indexCtrl = require('../controllers/index');

router.get('/',
    [
        validateMiddleware.checkShopDomain
    ],
    validateMiddleware.requiredInstall,
    indexCtrl.index,
);

module.exports = router;