'use strict';

const _ = require('lodash');
const express = require('express');
const router = express.Router();
const webhookCtrl = require('../controllers/webhook');

router.post('/uninstall', webhookCtrl.uninstall);

module.exports = router;