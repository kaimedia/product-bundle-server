'use strict';

const _ = require('lodash');
const config = require('../config');
const Shop = require('../models/shop');
const Bundle = require('../models/bundle');

function productBundleScript(req, res, next) {
    const shopDomain = req.query.shop;
    const styleURI = req.query.styleURI;
    const bundleData = `${config.S3_URI}/bundle_groups/${shopDomain}.json`;
    res.set('Content-Type', 'application/javascript');
    res.render('product_bundle', { apiURI: config.REG_URI, shopDomain: shopDomain, styleURI: styleURI, bundleData: bundleData });
}

function setTotalInfo(req, res, next) {
    const shopDomain = req.params.shop;
    const products = req.body.products;
}

function checkHasBundleGroup() {
    
}

function checkHasBundleUpsell() {

}

function generateDiscountCode(req, res, next) {
    const shopDomain = req.params.shop;
    const products = req.body.products;
    let productArr = [];

    products.forEach(el => productArr.push(Number(el.id)));

    // productIds: { $regex: String(productId), $options: "i" }
    Bundle.findOne({ shop: shopDomain, productIds: { $in: productArr }, status: 1 }).exec().then((response) => {
        let discountCode, bundleQuantity = 0, bundlePrice = 0;
        const bundle = response;

        if (!bundle) {
            return next({});
        }

        let bundleArr = _.map(bundle.products, element => {
            return _.assign(element, _.find(products, { id: element.id }));
        });

        bundleArr = _.filter(bundleArr, e => e.line_quantity);
        bundleArr.forEach(el => {
            bundleQuantity += el.line_quantity;
            bundlePrice += el.line_price;
        });

        bundlePrice = bundlePrice.toFixed(2);

        if (bundleQuantity < Object.keys(bundle.products).length) {
            return next({});
        }

        if (bundlePrice < bundle.old_total) {
            return next({});
        }

        discountCode = bundle.name;
        res.status(200).send({ code: discountCode, bundle: bundle });
    });
}

module.exports = { productBundleScript, generateDiscountCode };