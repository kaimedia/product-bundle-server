'use strict';

const _ = require('lodash');
const config = require('../config');
const Shopify = require('shopify-node-api');
const Shop = require('../models/shop');
const bundleService = require('../services/bundle');
const openSession = require('../helpers/shopify').openSession;

function uninstall(req, res, next) {
    const shopDomain = req.body.myshopify_domain;
    Shop.findOne({ shopify_domain: shopDomain }).exec().then((shop) => {
        const shopAPI = openSession(shop);
        Shop.findOneAndRemove({ shopify_domain: shopDomain }).exec().then(() => {
            bundleService.saveBundleJsonData(shopDomain, true);
            shopAPI.get('/admin/themes.json', (err, body) => {
                if (err) throw err;
                if (Object.keys(body).length) {
                    const currentTheme = _.find(body.themes, el => el.role === 'main');
                    shopAPI.get(`/admin/themes/${currentTheme.id}/assets.json?asset[key]=sections/footer.liquid`, (err, body) => {
                        if (err) throw err;
                        if (body.asset) {
                            let newData = body.asset.value;
                            newData = newData.replace(/<script rel='bundle-script'.*?>.*?<\/script>/ims, '');
                            shopAPI.put(`/admin/themes/${currentTheme.id}/assets.json`, {
                                asset: {
                                    key: 'sections/footer.liquid',
                                    value: newData,
                                }
                            }, (err, body) => {
                                if (err) throw err;
                                res.end();
                            });
                        }
                    });
                }
            });
        });
    });
}

module.exports = { uninstall };