'use strict';

const _ = require('lodash');
const fs = require('fs');
const AWS = require('aws-sdk');
const request = require('request');
const Promise = require('bluebird');
const config = require('../config');
const Shop = require('../models/shop');
const Shopify = require('shopify-node-api');
const bundleService = require('../services/bundle');
const generateNonce = require('../helpers/shopify').generateNonce;
const buildWebhook = require('../helpers/shopify').buildWebhook;
const moment = require('moment-timezone');
const SHOPIFY_API_KEY = config.SHOPIFY_API_KEY;
const SHOPIFY_SHARED_SECRET = config.SHOPIFY_SHARED_SECRET;
const AWS_ACCESS_KEY = config.AWS_ACCESS_KEY;
const AWS_SECRET_ACCESS_KEY = config.AWS_SECRET_ACCESS_KEY;
const APP_SCOPE = config.APP_SCOPE;
const REG_URI = config.REG_URI;
const S3_BUCKET = config.S3_BUCKET;
const S3_FOLDER = config.S3_FOLDER;
const S3_URI = config.S3_URI;

function index(req, res) {
    const shopDomain = req.query.shop;
    const nonce = generateNonce();
    const query = Shop.findOne({ shopify_domain: shopDomain }).exec();
    const shopAPI = new Shopify({
        shop: shopDomain,
        shopify_api_key: SHOPIFY_API_KEY,
        shopify_shared_secret: SHOPIFY_SHARED_SECRET,
        shopify_scope: APP_SCOPE,
        nonce,
        redirect_uri: `${REG_URI}/install/callback`,
    });
    const redirectURI = shopAPI.buildAuthURL();
    query.then((response) => {
        let save;
        const shop = response;
        if (!shop) {
            save = new Shop({ shopify_domain: shopDomain, nonce }).save();
        } else {
            shop.shopify_domain = shopDomain;
            shop.nonce = nonce;
            save = shop.save();
        }
        return save.then(() => res.redirect(redirectURI));
    });
}

function callback(req, res) {
    const params = req.query;
    const query = Shop.findOne({ shopify_domain: params.shop }).exec();
    query.then((result) => {
        const shop = result;
        const shopAPI = new Shopify({
            shop: params.shop,
            shopify_api_key: SHOPIFY_API_KEY,
            shopify_shared_secret: SHOPIFY_SHARED_SECRET,
            nonce: shop.nonce,
        });
        shopAPI.exchange_temporary_token(params, (err, data) => {
            if (err) {
                return next(err);
            }
            shopAPI.get('/admin/shop.json', (err, body) => {
                shop.custom_domain = body.shop.domain || body.shop.myshopify_domain;
                shop.hmac = params.hmac;
                shop.accessToken = data.access_token;
                shop.isActive = true;
                shop.save((err) => {
                    if (err) {
                        return next(err);
                    }
                    const addressPath = `${REG_URI}/webhook`;
                    const webhooks = config.WEBHOOKS;
                    const promises = [];
                    webhooks.forEach(wh => {
                        promises.push(__buildWebhook(wh.event, `${addressPath}/${wh.path}?shop=${shop.shopify_domain}`, shopAPI));
                    });
                    Promise.all(promises).then(() => {
                        buildAssets(params.shop).then(scriptURI => {
                            shopAPI.get('/admin/themes.json', (err, body) => {
                                if (err) throw err;
                                if (Object.keys(body).length) {
                                    const currentTheme = _.find(body.themes, el => el.role === 'main');
                                    shopAPI.get(`/admin/themes/${currentTheme.id}/assets.json?asset[key]=sections/footer.liquid`, (err, body) => {
                                        if (err) throw err;
                                        if (body.asset) {
                                            let newData = body.asset.value;
                                            newData = newData.replace(/<script rel='bundle-script'.*?>.*?<\/script>/ims, '');
                                            newData = `${newData} \n <script rel='bundle-script'> \n var _bundle_script = document.createElement('script'); \n _bundle_script.setAttribute('src','${scriptURI}?v=' + (new Date().getTime())); \n document.head.appendChild(_bundle_script); \n </script> \n`;
                                            shopAPI.put(`/admin/themes/${currentTheme.id}/assets.json`, {
                                                asset: {
                                                    key: 'sections/footer.liquid',
                                                    value: newData,
                                                }
                                            }, (err, body) => {
                                                if (err) throw err;
                                                bundleService.saveBundleJsonData(shop.shopify_domain);

                                                if (shop.scriptId && shop.scriptId !== null) {
                                                    shopAPI.delete(`/admin/script_tags/${shop.scriptId}.json`, () => {
                                                        shop.scriptId = null;
                                                        shop.save((err) => {
                                                            if (err) throw err;
                                                        });
                                                    });
                                                }

                                                res.redirect(`https://${shop.shopify_domain}/admin/apps`);
                                            });
                                        }
                                    });
                                }
                            });
                        }, err => next(err));
                    });
                });
            });
        });
    });
}

function __buildWebhook(event, destPath, shopAPI) {
    return new Promise((resolve) => {
        buildWebhook(event, destPath, shopAPI, () => {
            return resolve();
        });
    });
}

function buildAssets(shop) {
    return new Promise((resolve, reject) => {
        const S3 = new AWS.S3({
            accessKeyId: AWS_ACCESS_KEY,
            secretAccessKey: AWS_SECRET_ACCESS_KEY,
        });
        const timestamp = moment().unix();
        const bucketName = S3_BUCKET;
        const bucketFolder = S3_FOLDER;
        const bucketUrl = S3_URI;
        const styleURI = `${bucketUrl}/${timestamp}.css`;
        const scriptURI = `${bucketUrl}/${timestamp}.js`;
        request({
            url: `${REG_URI}/assets/js/product_bundle.js`,
            method: 'GET',
            headers: {},
            qs: {
                shop: shop,
                styleURI: styleURI,
            }
        }, (error, response, js) => {
            if (error) throw error;
            fs.readFile('public/css/product_bundle.css', (err, css) => {
                if (err) throw err;
                S3.upload({
                    Bucket: bucketName,
                    Key: `${bucketFolder}/${timestamp}.css`,
                    Body: new Buffer(css, 'binary'),
                    ContentType: 'text/css',
                    ACL: 'public-read',
                }, (err, data) => {
                    if (err) throw err;
                    console.log(`CSS uploaded successfully at ${data.Location}`);
                });
                S3.upload({
                    Bucket: bucketName,
                    Key: `${bucketFolder}/${timestamp}.js`,
                    Body: new Buffer(js, 'binary'),
                    ContentType: 'application/javascript',
                    ACL: 'public-read',
                }, (err, data) => {
                    if (err) throw err;
                    console.log(`JS uploaded successfully at ${data.Location}`);
                    resolve(scriptURI);
                });
            });
        });
    });
}

module.exports = { index, callback };