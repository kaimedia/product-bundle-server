'use strict';

const _ = require('lodash');
const logger = require('../lib/logger');
const config = require('../config');
const Shopify = require('shopify-node-api');
const Shop = require('../models/shop');
const Bundle = require('../models/bundle');
const Promise = require('bluebird');

function list(req, res, next) {
    const shopDomain = req.params.shop;
    const bundleType = req.params.type;
    const addProductType = req.params.add_type;
    const query = Shop.findOne({ shopify_domain: shopDomain }).exec();
    query.then((result) => {
        const shop = result;
        const shopAPI = new Shopify({
            shop: shopDomain,
            shopify_api_key: config.SHOPIFY_API_KEY,
            shopify_shared_secret: config.SHOPIFY_SHARED_SECRET,
            access_token: shop.accessToken,
        });
        shopAPI.get('/admin/products.json', { limit: 250, published_status: 'published', fields: 'id,title,handle,image,images,variants,vendor,product_type,template_suffix' }, (err, results) => {
            bundleGroups(shopDomain, bundleType, addProductType).then((productIds) => {
                // const products = results['products'].filter((e) => e.template_suffix == 'bundle');
                const products = results['products'];
                products.forEach(product => {
                    if (_.includes(productIds, product.id)) {
                        product.disabled = true;
                    } else {
                        product.disabled = false;
                    }

                    product.images = _.map(product.images, img => _.pick(img, ['id', 'src']));
                    product.variants.forEach(v => {
                        v.image_url = product.image ? product.image.src : '';
                        let variantImage = _.find(product.images, ['id', v.image_id]);
                        if (variantImage) v.image_url = variantImage.src;
                    });

                    return product;
                });
                res.status(200).send(products);
            }, err => next(err));
        });
    });
}

function bundleGroups(shopDomain, bundleType, addProductType) {
    return new Promise((resolve, reject) => {
        let productIds = [];
        Bundle.find({ shop: shopDomain, type: bundleType }).exec().then((bundles) => {
            if (bundles) {
                bundles.forEach(bundle => {

                    switch (bundleType) {
                        case config.BUNDLE_TYPES[0]:
                            bundle.products.forEach(e => {
                                productIds.push(e.id);
                            });
                            break;

                        case config.BUNDLE_TYPES[1]:
                            bundle.offer.products.forEach(e => {
                                productIds.push(e.id);
                            });
                            break;

                        case config.BUNDLE_TYPES[2]:
                            switch (addProductType) {
                                case 'trigger':
                                    bundle.cross_sell.triggerProducts.forEach(e => {
                                        productIds.push(e.id);
                                    });
                                    break;

                                case 'offer':
                                    bundle.cross_sell.offerProducts.forEach(e => {
                                        productIds.push(e.id);
                                    });
                                    break;
                            }
                            break;

                        default:
                            break;
                    }

                });
                resolve(productIds);
            }
        });
    });
}

exports.list = list;
