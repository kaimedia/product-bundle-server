'use strict';

const _ = require('lodash');
const config = require('../config');
const Shop = require('../models/shop');
const Bundle = require('../models/bundle');
const openSession = require('../helpers/shopify').openSession;
const moment = require('moment-timezone');
// moment.tz.setDefault(config.TIMEZONE);

function productBundleScript(req, res, next) {
    const shopDomain = req.query.shop;
    const styleURI = req.query.styleURI;
    const bundleData = `${config.S3_URI}/bundle_groups/${shopDomain}.json`;
    res.set('Content-Type', 'application/javascript');
    res.render('product_bundle', { apiURI: config.REG_URI, shopDomain: shopDomain, styleURI: styleURI, bundleData: bundleData });
}

function generateDiscountCode(req, res, next) {
    const shopDomain = req.params.shop;
    const originTotal = Math.floor(req.body.originalTotal);
    const subTotal = Number(req.body.subTotal);
    const savingTotal = Number(req.body.savingTotal);
    Shop.findOne({ shopify_domain: shopDomain, isActive: true }).exec((err, shop) => {
        if (err) return next(err);
        const shopAPI = openSession(shop);
        const disCountName = `BUNDLE_DISCOUNT_MG${moment().unix()}`;
        const discountValue = -Math.abs(savingTotal);
        const startsAt = moment().subtract(5, 'minutes').toISOString();
        const endsAt = moment().add(4, 'hours').toISOString();
        shopAPI.post('/admin/price_rules.json', {
            price_rule: {
                title: disCountName,
                target_type: 'line_item',
                target_selection: 'all',
                allocation_method: 'across',
                value_type: 'fixed_amount',
                value: discountValue,
                usage_limit: 1,
                once_per_customer: true,
                customer_selection: 'all',
                prerequisite_subtotal_range: {
                    greater_than_or_equal_to: originTotal,
                },
                prerequisite_quantity_range: null,
                starts_at: startsAt,
                ends_at: endsAt,
            }
        }, (err, data) => {
            if (err) return next(err);
            shopAPI.post(`/admin/price_rules/${data.price_rule.id}/discount_codes.json`, {
                discount_code: {
                    code: disCountName,
                }
            }, (err) => {
                if (err) return next(err);
                return res.status(200).send({ code: disCountName })
            });
        });
    });
}

module.exports = { productBundleScript, generateDiscountCode };