'use strict';

const verifyOAuth = require('../helpers/shopify').verifyOAuth;
const config = require('../config');
const Shop = require('../models/shop');
const APP_URI = config.APP_URI;

function index(req, res, next) {
    const query = Object.keys(req.query).map((key) => `${key}=${req.query[key]}`).join('&');
    Shop.findOne({ shopify_domain: req.query.shop, isActive: true }, (err, shop) => {
        if (!shop) {
            return res.redirect(`/install/?${query}`);
        }
        if (req.query.hmac && req.query.hmac === shop.hmac) {
            return res.redirect(`${APP_URI}?${query}`);
        }
        if (verifyOAuth(req.query)) {
            return res.redirect(`${APP_URI}?${query}`);
        }
        return res.end();
    });
}

module.exports = { index };