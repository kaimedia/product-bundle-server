'use strict';

const _ = require('lodash');
const AWS = require('aws-sdk');
const config = require('../config');
const logger = require('../lib/logger');
const Shop = require('../models/shop');
const Bundle = require('../models/bundle');
const bundleService = require('../services/bundle');

function get(req, res, next) {
    Bundle.findOne({ shop: req.params.shop, _id: req.params.id }).exec((err, bundle) => {
        if (err) return next(err);
        return res.status(200).send(bundle);
    });
}

function list(req, res, next) {
    Bundle.find({ shop: req.params.shop }).exec((err, bundles) => {
        if (err) return next(err);
        return res.status(200).send(bundles);
    });
}

function create(req, res, next) {
    bundleService.create(req.body).then(() => res.status(200).send({ success: true }));
}

function update(req, res, next) {
    bundleService.update(req.params.id, req.body).then((data) => res.status(200).send(data));
}

function status(req, res, next) {
    bundleService.status(req.params.shop, req.params.id, req.body.status).then((data) => res.status(200).send(data));
}

function remove(req, res, next) {
    bundleService.remove(req.params.shop, req.params.id).then(() => res.status(200).send({ success: true }));
}

exports.get = get;
exports.list = list;
exports.create = create;
exports.update = update;
exports.status = status;
exports.remove = remove;