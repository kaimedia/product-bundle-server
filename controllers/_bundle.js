'use strict';

const _ = require('lodash');
const AWS = require('aws-sdk');
const config = require('../config');
const logger = require('../lib/logger');
const Shop = require('../models/shop');
const Bundle = require('../models/bundle');
const Shopify = require('shopify-node-api');
const openSession = require('../helpers/shopify').openSession;
const moment = require('moment-timezone');
moment.tz.setDefault(config.TIMEZONE);
const AWS_ACCESS_KEY = config.AWS_ACCESS_KEY;
const AWS_SECRET_ACCESS_KEY = config.AWS_SECRET_ACCESS_KEY;
const S3 = new AWS.S3({
    accessKeyId: AWS_ACCESS_KEY,
    secretAccessKey: AWS_SECRET_ACCESS_KEY,
});

function get(req, res, next) {
    Bundle.findOne({ shop: req.params.shop, _id: req.params.id }).exec()
        .then((bundle) => res.status(200).send(bundle));
}

function list(req, res, next) {
    Bundle.find({ shop: req.params.shop }).exec()
        .then((bundles) => res.status(200).send(bundles));
}

function create(req, res, next) {
    Bundle.findOne({ name: req.body.name, shop: req.body.shop }, (err, duplicated) => {
        if (err) {
            return next(err);
        }

        if (duplicated) {
            return res.status(500).send({ message: "Name must be unique. Please try a different name." });
        }

        if (!duplicated) {
            const products = reMakeProducts(req.body);
            new Bundle({
                type: req.body.type,
                name: req.body.name,
                products: products,
                productIds: indexProductIds(products),
                discount_method: req.body.discount_method,
                percent: req.body.percent,
                fixed_price: req.body.fixed_price,
                total: req.body.total,
                old_total: req.body.old_total,
                offer: req.body.offer,
                shop: req.body.shop,
                status: 1,
            }).save((err, bundle) => {
                if (err) {
                    return next(err);
                }
                let save;
                saveBundleJsonData(req.body.shop);
                const query = Shop.findOne({ shopify_domain: req.body.shop }).exec();
                query.then((result) => {
                    const shop = result;
                    let productArr = [];
                    const shopAPI = openSession(shop);
                    let _value = bundle.percent;
                    if (bundle.discount_method === 'fixed_amount') {
                        _value = req.body.fixed_price;
                    }
                    _value = -Math.abs(_value);
                    products.forEach(prod => productArr.push(prod.id));

                    shopAPI.post('/admin/price_rules.json', {
                        price_rule: {
                            title: bundle.name,
                            target_type: 'line_item',
                            target_selection: 'entitled',
                            allocation_method: 'across',
                            value_type: bundle.discount_method,
                            value: _value,
                            customer_selection: 'all',
                            entitled_product_ids: productArr,
                            prerequisite_subtotal_range: {
                                greater_than_or_equal_to: req.body.old_total,
                            },
                            prerequisite_quantity_range: null,
                            starts_at: moment().toISOString(),
                        }
                    }, (err, data) => {
                        if (err) return next(err);
                        bundle.price_rule_id = data.price_rule.id;
                        shopAPI.post(`/admin/price_rules/${data.price_rule.id}/discount_codes.json`, {
                            discount_code: {
                                code: bundle.name,
                            }
                        }, (err, data) => {
                            if (err) return next(err);
                            bundle.discount_id = data.discount_code.id;
                            save = bundle.save();
                            save.then(() => res.status(200).send(bundle));
                        });
                    });
                });
            });
        }
    });
}

function update(req, res, next) {
    const products = reMakeProducts(req.body);
    Bundle.findByIdAndUpdate(req.params.id, {
        name: req.body.name,
        products: products,
        productIds: indexProductIds(products),
        discount_method: req.body.discount_method,
        percent: req.body.percent,
        fixed_price: req.body.fixed_price,
        total: req.body.total,
        old_total: req.body.old_total,
        offer: req.body.offer,
        status: req.body.status,
    }).exec().then((bundle) => {
        saveBundleJsonData(req.params.shop);
        const query = Shop.findOne({ shopify_domain: req.params.shop }).exec();
        query.then((result) => {
            const shop = result;
            let productArr = [];
            const shopAPI = openSession(shop);
            let _value = req.body.percent;
            if (req.body.discount_method === 'fixed_amount') {
                _value = req.body.fixed_price;
            }
            _value = -Math.abs(_value);
            products.forEach(prod => productArr.push(prod.id));
            if (bundle.price_rule_id) {
                let priceRuleData = {
                    id: bundle.price_rule_id,
                    title: req.body.name,
                    value_type: req.body.discount_method,
                    value: _value,
                    entitled_product_ids: productArr,
                    prerequisite_subtotal_range: {
                        greater_than_or_equal_to: req.body.old_total,
                    },
                    prerequisite_quantity_range: null
                }
                if (req.body.status !== bundle.status) {
                    priceRuleData.ends_at = null;
                    if (req.body.status === 0) {
                        priceRuleData.ends_at = moment().toISOString();
                    }
                }
                shopAPI.put(`/admin/price_rules/${bundle.price_rule_id}.json`, {
                    price_rule: priceRuleData,
                }, (err) => {
                    if (err) {
                        return next(err);
                    }
                    if (bundle.discount_id) {
                        shopAPI.put(`/admin/price_rules/${bundle.price_rule_id}/discount_codes/${bundle.discount_id}.json`, {
                            discount_code: {
                                id: bundle.discount_id,
                                code: req.body.name,
                            }
                        }, (err) => {
                            if (err) {
                                return next(err);
                            }
                            res.status(200).send(bundle);
                        });
                    }
                });
            }
        });
    });
}

function remove(req, res, next) {
    Bundle.findByIdAndRemove(req.params.id).exec().then((bundle) => {
        saveBundleJsonData(req.params.shop);
        if (bundle.price_rule_id) {
            const query = Shop.findOne({ shopify_domain: req.params.shop }).exec();
            query.then((result) => {
                const shop = result;
                const shopAPI = openSession(shop);
                shopAPI.delete(`/admin/price_rules/${bundle.price_rule_id}.json`, (err) => {
                    if (err) return next(err);
                    if (bundle.discount_id) {
                        shopAPI.delete(`admin/price_rules/${bundle.price_rule_id}/discount_codes/${bundle.discount_id}.json`, (err) => {
                            res.status(200).send({});
                        });
                    }
                });
            });
        }
    });
}

function reMakeProducts(data) {
    data.products = _.map(data.products, product => {
        product.variants = _.map(product.variants, variant => {
            let newVariant = _.pick(variant, ['id', 'product_id', 'title', 'price']);
            let bundle_price = Number(variant.price);
            if (data.percent) {
                bundle_price = _.round(Number(variant.price) - ((Number(variant.price) * data.percent) / 100), 2).toFixed(2);
            }
            newVariant.bundle_price = bundle_price;
            return newVariant;
        });
        return product;
    });
    return data.products;
}

function indexProductIds(products) {
    let productArr = [];
    products.forEach(el => {
        productArr.push(el.id);
    });
    return productArr.join("|");
}

function saveBundleJsonData(shop, empty = false) {
    const query = Bundle.find({ shop: shop, status: 1 }).exec();
    query.then((response) => {
        let body = response;
        if (empty) {
            body = [];
        }
        S3.upload({
            Bucket: `${config.S3_BUCKET}`,
            Key: `${config.S3_FOLDER}/bundle_groups/${shop}.json`,
            Body: Buffer.from(JSON.stringify(body)),
            ContentType: 'application/json',
            ACL: 'public-read',
        }, (err, data) => {
            if (err) throw err;
            console.log(`JSON uploaded successfully at ${data.Location}`);
        });
    });
}

exports.get = get;
exports.list = list;
exports.create = create;
exports.update = update;
exports.remove = remove;
exports.saveBundleJsonData = saveBundleJsonData;