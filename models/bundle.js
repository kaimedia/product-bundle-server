'user strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var BundleGroupSchema = mongoose.Schema({
    name: {
        type: String,
        require: true,
    },
    shop: {
        type: String,
        require: true,
    },
    type: String,
    products: Object,
    productIds: {
        type: String,
        default: "",
    },
    discount_method: String,
    percent: Number,
    fixed_price: Number,
    total: Number,
    old_total: Number,
    price_rule_id: Number,
    discount_id: Number,
    status: {
        type: Number,
        default: 0,
    },
    created_at: {
        type: Date,
        default: Date.now,
    },
    updated_at: {
        type: Date,
        default: Date.now,
    },
    cross_sell: {
        type: Object,
        default: {
            triggerProductIds: "",
            offerProductIds: "",
            triggerProducts: [],
            offerProducts: [],
            discountValue: "",
        }
    },
    offer: {
        type: Object,
        default: {
            productIds: "",
            products: [],
            price_levels: [],
            hasFree: false,
            hasSale: false,
        }
    }
});

BundleGroupSchema.pre('save', function (next) {
    if (!this.updated_at)
        this.updated_at = Date.now();
    next();
});

module.exports = mongoose.model('Bundle_Group', BundleGroupSchema);