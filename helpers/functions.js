'use strict';

const Shop = require('../models/shop');
const openSession = require('../helpers/shopify').openSession;
const config = require('../config');

function shopAPI(shopDomain) {
    return new Promise((resolve, reject) => {
        const query = Shop.findOne({ shopify_domain: shopDomain }).exec((err, shop) => {
            if (err) throw err;
            const shopAPI = openSession(shop);
            resolve(shopAPI);
        });
    });
}

function getVariants(shopAPI, productId) {
    return new Promise((resolve, reject) => {
        shopAPI.get(`/admin/products/${productId}/variants.json`, (err, res) => {
            if (err) throw err;
            resolve(res.variants);
        });
    });
}

function removeVariant(shopAPI, productId, variantId) {
    return new Promise((resolve, reject) => {
        shopAPI.delete(`/admin/products/${productId}/variants/${variantId}.json`, (err, res) => {
            if (err) throw err;
            resolve(res);
        });
    });
}

function searchVariants(variants, type) {
    let originVariants = [];
    let customVariants = [];

    variants.forEach(e => {
        let pattCustom;
        let pattOrigin = new RegExp(/[!@#$%^&*()_+=\[\]{};':"\\|,.<>?]/g);

        if (!pattOrigin.test(e.title)) {
            originVariants.push(e);
        }
        switch (type) {
            case config.BUNDLE_TYPES[0]:
                break;

            case config.BUNDLE_TYPES[1]:
                pattCustom = new RegExp(/BUNDLE_UPSELL/g);
                if (pattCustom.test(e.title)) {
                    customVariants.push(e);
                }
                break;

            case config.BUNDLE_TYPES[2]:
                pattCustom = new RegExp(/BUNDLE (\d+)\% OFF/g);
                if (pattCustom.test(e.title)) {
                    customVariants.push(e);
                }
                break;
        }
    });
    return {
        originVariants,
        customVariants,
    };
}

function removeDefaultTitle(title) {
    if (title && title.indexOf("Default") != -1) {
        title = title.replace("Default Title", "");
    }
    return title;
}

function checkVariantExisted(variantList, customTitle) {
    return new Promise((resolve, reject) => {
        const variant = variantList.find(function (e) { return e.title.indexOf(customTitle) != -1 });
        if (variant) {
            return resolve(variant);
        }
        resolve(null);
    });
}

module.exports = { shopAPI, getVariants, removeVariant, searchVariants, removeDefaultTitle, checkVariantExisted };