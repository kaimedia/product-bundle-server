'use strict';

const _ = require('lodash');
const AWS = require('aws-sdk');
const config = require('../config');
const logger = require('../lib/logger');
const Shop = require('../models/shop');
const Bundle = require('../models/bundle');
const Shopify = require('shopify-node-api');
const openSession = require('../helpers/shopify').openSession;
const Func = require('../helpers/functions');

const S3 = new AWS.S3({
    accessKeyId: config.AWS_ACCESS_KEY,
    secretAccessKey: config.AWS_SECRET_ACCESS_KEY,
});

function create(data) {
    return new Promise((resolve, reject) => {
        switch (data.type) {
            case config.BUNDLE_TYPES[0]:
                createGroup(data).then(() => resolve());
                break;

            case config.BUNDLE_TYPES[1]:
                createOffer(data).then(() => resolve());
                break;

            case config.BUNDLE_TYPES[2]:
                createCrossSell(data).then(() => resolve());
                break;

            default:
                break;
        }
    });
}

function createGroup(data) {
    return new Promise((resolve, reject) => {
        const products = handleProducts(data);
        new Bundle({
            type: data.type,
            name: data.name,
            products: products,
            productIds: setIndexProduct(products),
            discount_method: data.discount_method,
            percent: data.percent,
            fixed_price: data.fixed_price,
            total: data.total,
            old_total: data.old_total,
            shop: data.shop,
            status: 1,
        }).save((err, bundle) => {
            if (err) {
                return reject(err);
            }
            saveBundleJsonData(data.shop);
            return resolve();
        });
    });
}

function createOffer(data) {
    return new Promise((resolve, reject) => {
        data.offer.productIds = setIndexProduct(data.offer.products);
        new Bundle({
            type: data.type,
            name: data.name,
            offer: data.offer,
            shop: data.shop,
            status: 1,
        }).save((err, bundle) => {
            if (err) return reject(err);
            createVariants(data).then((res) => {
                saveCustomVariants(bundle, res).then(() => {
                    saveBundleJsonData(data.shop);
                    resolve();
                }, err => reject(err));
            }, err => reject(err));
        });
    });
}

function createCrossSell(data) {
    return new Promise((resolve, reject) => {
        data.cross_sell.triggerProductIds = setIndexProduct(data.cross_sell.triggerProducts);
        data.cross_sell.offerProductIds = setIndexProduct(data.cross_sell.offerProducts);
        new Bundle({
            type: data.type,
            name: data.name,
            cross_sell: data.cross_sell,
            shop: data.shop,
            status: 1,
        }).save((err, bundle) => {
            if (err) return reject(err);
            createVariants(data).then((res) => {
                saveCustomVariants(bundle, res).then(() => {
                    saveBundleJsonData(data.shop);
                    resolve();
                }, err => reject(err));
            }, err => reject(err));
        });
    });
}

function update(id, data) {
    return new Promise((resolve, reject) => {
        switch (data.type) {
            case config.BUNDLE_TYPES[0]:
                updateGroup(id, data).then((bundle) => resolve(bundle));
                break;

            case config.BUNDLE_TYPES[1]:
                updateOffer(id, data).then((bundle) => resolve(bundle));
                break;

            case config.BUNDLE_TYPES[2]:
                updateCrossSell(id, data).then((bundle) => resolve(bundle));
                break;

            default:
                break;
        }
    });
}

function updateGroup(id, data) {
    return new Promise((resolve, reject) => {
        const products = handleProducts(data);
        Bundle.findByIdAndUpdate(id, {
            name: data.name,
            products: products,
            productIds: setIndexProduct(products),
            discount_method: data.discount_method,
            percent: data.percent,
            fixed_price: data.fixed_price,
            total: data.total,
            old_total: data.old_total,
            offer: data.offer,
            status: data.status,
        }, { new: true }).exec((err, bundle) => {
            if (err) {
                return reject(err);
            }
            saveBundleJsonData(data.shop);
            return resolve(bundle);
        });
    });
}

function updateOffer(id, data) {
    return new Promise((resolve, reject) => {
        data.offer.productIds = setIndexProduct(data.offer.products);
        Bundle.findByIdAndUpdate(id, {
            name: data.name,
            offer: data.offer,
            status: data.status,
        }, { new: true }).exec((err, bundle) => {
            if (err) {
                return reject(err);
            }
            createVariants(data).then((res) => {
                saveCustomVariants(bundle, res).then(() => {
                    saveBundleJsonData(data.shop);
                    return resolve(bundle);
                }, err => reject(err));
            }, err => reject(err));
        });
    });
}

function updateCrossSell(id, data) {
    return new Promise((resolve, reject) => {

        data.cross_sell.triggerProductIds = setIndexProduct(data.cross_sell.triggerProducts);
        data.cross_sell.offerProductIds = setIndexProduct(data.cross_sell.offerProducts);
        Bundle.findByIdAndUpdate(id, {
            name: data.name,
            cross_sell: data.cross_sell,
            status: data.status,
        }, { new: true }).exec((err, bundle) => {
            if (err) {
                return reject(err);
            }
            createVariants(data).then((res) => {
                saveCustomVariants(bundle, res).then(() => {
                    saveBundleJsonData(data.shop);
                    return resolve(bundle);
                }, err => reject(err));
            }, err => reject(err));
        });
    });
}

function status(shop, id, status) {
    return new Promise((resolve, reject) => {
        Bundle.findByIdAndUpdate(id, { status: status }, { new: true }).exec((err, bundle) => {
            if (err) {
                return reject(err);
            }
            saveBundleJsonData(shop);
            return resolve(bundle);
        });
    });
}

function remove(shop, id) {
    return new Promise((resolve, reject) => {
        Bundle.findByIdAndRemove(id).exec((err, bundle) => {
            if (err) return reject(err);
            saveBundleJsonData(shop);
            removeVariants(bundle).then(() => resolve());
        });
    });
}

function handleProducts(data) {
    data.products = _.map(data.products, product => {
        product.variants = _.map(product.variants, variant => {
            let newVariant = _.pick(variant, ['id', 'product_id', 'title', 'price']);
            let bundle_price = Number(variant.price);
            if (data.percent) {
                bundle_price = _.round(Number(variant.price) - ((Number(variant.price) * data.percent) / 100), 2).toFixed(2);
            }
            newVariant.bundle_price = bundle_price;
            return newVariant;
        });
        return product;
    });
    return data.products;
}

function setIndexProduct(products) {
    let productArr = [];
    products.forEach(el => {
        productArr.push(el.id);
    });
    return productArr.join("|");
}

function saveBundleJsonData(shop, empty = false) {
    const query = Bundle.find({ shop: shop, status: 1 }).exec();
    query.then((response) => {
        let body = response;
        if (empty) {
            body = [];
        }
        S3.upload({
            Bucket: `${config.S3_BUCKET}`,
            Key: `${config.S3_FOLDER}/bundle_groups/${shop}.json`,
            Body: Buffer.from(JSON.stringify(body)),
            ContentType: 'application/json',
            ACL: 'public-read',
        }, (err, data) => {
            if (err) throw err;
            console.log(`JSON uploaded successfully at ${data.Location}`);
        });
    });
}

function createVariants(data) {
    return new Promise((resolve, reject) => {
        let promises = [], products = [];
        switch (data.type) {
            case config.BUNDLE_TYPES[0]:
                products = data.products;
                break;

            case config.BUNDLE_TYPES[1]:
                products = data.offer.products;
                break;

            case config.BUNDLE_TYPES[2]:
                products = _.filter(data.cross_sell.offerProducts, item => item.recommend && item.recommend.show == true);
                break;
        }
        Func.shopAPI(data.shop).then((shopAPI) => {
            if (!products.length) return resolve([]);
            products.forEach((product) => promises.push(updateVariantsOnProduct(shopAPI, data, product)));
            Promise.all(promises).then(results => resolve(results)).catch(err => reject(err));
        }, err => reject(err));
    });
}

function updateVariantsOnProduct(shopAPI, data, product) {
    return new Promise((resolve, reject) => {
        const productId = product.id;
        Func.getVariants(shopAPI, productId).then((variants) => {
            if (_.isEmpty(variants)) return;
            createCustomVariants(shopAPI, data, productId, variants).then((res) => {
                resolve({
                    productId: productId,
                    variants: Func.searchVariants(variants).originVariants,
                    offerVariants: res.results,
                });
            }, err => reject(err));
        }, err => reject(err));
    });
}

function createCustomVariants(shopAPI, data, productId, variants) {
    return new Promise((resolve, reject) => {
        let promises = [], time_out = 0;
        const originVariants = Func.searchVariants(variants).originVariants;

        switch (data.type) {
            case config.BUNDLE_TYPES[0]:
                break;

            case config.BUNDLE_TYPES[1]:
                let percents = [];
                let levels = data.offer.price_levels;
                levels.push({ value: 0 });
                levels.forEach(level => {
                    if (percents.indexOf(level.value) <= -1) {
                        percents.push(level.value);
                        originVariants.forEach((variant) => promises.push(offerSaveVariant(shopAPI, productId, variants, variant, level.value, time_out)));
                        time_out += 200;
                    }
                });
                break;

            case config.BUNDLE_TYPES[2]:
                originVariants.forEach((variant) => {
                    promises.push(crossSellSaveVariant(shopAPI, productId, variants, variant, data.cross_sell.discountValue, time_out));
                    time_out += 200;
                });
                break;
        }

        Promise.all(promises).then(results => {
            resolve({ results: results });
        }).catch(err => reject(err));
    });
}

function offerSaveVariant(shopAPI, productId, variants, variant, percent, time_out) {
    return new Promise((resolve, reject) => {
        let optionArr = [];
        let customVariantTitle = `BUNDLE_UPSELL`;
        if (percent != 0) customVariantTitle = `BUNDLE_UPSELL ${percent}% OFF`;
        let option1 = variant.option1 ? `${variant.option1} / ${customVariantTitle}` : variant.option1;
        let option2 = variant.option2 ? `${variant.option2} / ${customVariantTitle}` : variant.option2;
        let option3 = variant.option3 ? `${variant.option3} / ${customVariantTitle}` : variant.option3;
        option1 = Func.removeDefaultTitle(option1);
        option2 = Func.removeDefaultTitle(option2);
        option3 = Func.removeDefaultTitle(option3);
        if (option1) optionArr.push(option1);
        if (option2) optionArr.push(option2);
        if (option3) optionArr.push(option3);
        const customTitle = optionArr.join(' / ');
        const price = (parseFloat(variant.price) - (parseFloat(variant.price) * (percent / 100))).toFixed(2);
        const position = 50 + Math.floor(Math.random() * 150);

        let dataVariant = {
            variant: {
                option1: option1,
                option2: option2,
                option3: option3,
                price: price,
                sku: variant.sku,
                position: position,
                inventory_policy: variant.inventory_policy,
                compare_at_price: variant.compare_at_price,
                fulfillment_service: variant.fulfillment_service,
                inventory_management: variant.inventory_management,
                taxable: variant.taxable,
                barcode: variant.barcode,
                grams: variant.grams,
                image_id: variant.image_id,
                inventory_quantity: variant.inventory_quantity,
                weight: variant.weight,
                weight_unit: variant.weight_unit,
                old_inventory_quantity: variant.old_inventory_quantity,
                requires_shipping: variant.requires_shipping,
            }
        };

        Func.checkVariantExisted(variants, customTitle).then((variantExisted) => {
            setTimeout(() => {
                if (variantExisted) {
                    dataVariant.variant.id = variantExisted.id;
                    shopAPI.put(`/admin/variants/${variantExisted.id}.json`, dataVariant, (err, res) => {
                        if (err) return logger.error(err);
                        const variantId = res.variant.id;
                        const variantTitle = res.variant.title;
                        resolve({
                            origin_variant_id: variant.id,
                            custom_variant_id: variantId,
                            price: parseFloat(price),
                            origin_price: parseFloat(variant.price),
                            value: parseFloat(percent),
                            title: variantTitle,
                        });
                    });
                } else {
                    shopAPI.post(`/admin/products/${productId}/variants.json`, dataVariant, (err, res) => {
                        if (err) throw err;
                        const variantId = res.variant.id;
                        const variantTitle = res.variant.title;
                        shopAPI.post(`/admin/products/${productId}/variants/${variantId}/metafields.json`, { metafield: { namespace: 'kai_bundle', key: 'hide_variant', value: '1', value_type: 'string' } }, (err, body) => { });
                        shopAPI.post(`/admin/products/${productId}/variants/${variantId}/metafields.json`, { metafield: { namespace: 'kai_bundle', key: 'free_variant', value: '1', value_type: 'string' } }, (err, body) => { });
                        shopAPI.post(`/admin/products/${productId}/variants/${variantId}/metafields.json`, { metafield: { namespace: 'kai_bundle', key: 'upsell_variant', value: '1', value_type: 'string' } }, (err, body) => { });
                        resolve({
                            origin_variant_id: variant.id,
                            custom_variant_id: variantId,
                            price: parseFloat(price),
                            origin_price: parseFloat(variant.price),
                            value: parseFloat(percent),
                            title: variantTitle,
                        });
                    });
                }
            }, time_out);
        });

    });
}

function crossSellSaveVariant(shopAPI, productId, variants, variant, percent, time_out) {
    return new Promise((resolve, reject) => {
        let optionArr = [];
        const customVariantTitle = `BUNDLE ${percent}% OFF`;
        let option1 = variant.option1 ? `${variant.option1} / ${customVariantTitle}` : variant.option1;
        let option2 = variant.option2 ? `${variant.option2} / ${customVariantTitle}` : variant.option2;
        let option3 = variant.option3 ? `${variant.option3} / ${customVariantTitle}` : variant.option3;
        option1 = Func.removeDefaultTitle(option1);
        option2 = Func.removeDefaultTitle(option2);
        option3 = Func.removeDefaultTitle(option3);
        if (option1) optionArr.push(option1);
        if (option2) optionArr.push(option2);
        if (option3) optionArr.push(option3);
        const customTitle = optionArr.join(' / ');
        const price = (parseFloat(variant.price) - (parseFloat(variant.price) * (percent / 100))).toFixed(2);
        const position = 60 + Math.floor(Math.random() * 150);

        let dataVariant = {
            variant: {
                option1: option1,
                option2: option2,
                option3: option3,
                price: price,
                sku: variant.sku,
                position: position,
                inventory_policy: variant.inventory_policy,
                compare_at_price: variant.compare_at_price,
                fulfillment_service: variant.fulfillment_service,
                inventory_management: variant.inventory_management,
                taxable: variant.taxable,
                barcode: variant.barcode,
                grams: variant.grams,
                image_id: variant.image_id,
                inventory_quantity: variant.inventory_quantity,
                weight: variant.weight,
                weight_unit: variant.weight_unit,
                old_inventory_quantity: variant.old_inventory_quantity,
                requires_shipping: variant.requires_shipping,
            }
        };

        Func.checkVariantExisted(variants, customTitle).then((variantExisted) => {
            setTimeout(() => {
                if (variantExisted) {
                    dataVariant.variant.id = variantExisted.id;
                    shopAPI.put(`/admin/variants/${variantExisted.id}.json`, dataVariant, (err, res) => {
                        if (err) return logger.error(err);
                        const variantId = res.variant.id;
                        const variantTitle = res.variant.title;
                        resolve({
                            origin_variant_id: variant.id,
                            custom_variant_id: variantId,
                            price: parseFloat(price),
                            origin_price: parseFloat(variant.price),
                            value: parseFloat(percent),
                            title: variantTitle,
                        });
                    });
                } else {
                    shopAPI.post(`/admin/products/${productId}/variants.json`, dataVariant, (err, res) => {
                        if (err) return logger.error(err);
                        const variantId = res.variant.id;
                        const variantTitle = res.variant.title;
                        shopAPI.post(`/admin/products/${productId}/variants/${variantId}/metafields.json`, { metafield: { namespace: 'kai_bundle', value_type: 'string', key: 'hide_variant', value: '1' } }, (err, body) => { });
                        shopAPI.post(`/admin/products/${productId}/variants/${variantId}/metafields.json`, { metafield: { namespace: 'kai_bundle', key: 'cross_sell_variant', value_type: 'string', value: '1' } }, (err, body) => { });
                        resolve({
                            origin_variant_id: variant.id,
                            custom_variant_id: variantId,
                            price: parseFloat(price),
                            origin_price: parseFloat(variant.price),
                            value: parseFloat(percent),
                            title: variantTitle,
                        });
                    });
                }
            }, time_out);
        });

    });
}

function saveCustomVariants(bundle, responses) {
    return new Promise((resolve, reject) => {
        let products = [], data = {};
        switch (bundle.type) {
            case config.BUNDLE_TYPES[0]:
                products = mapOfferVariants(bundle.products, responses);
                data = { 'products': products };
                break;

            case config.BUNDLE_TYPES[1]:
                products = mapOfferVariants(bundle.offer.products, responses);
                data = { 'offer.products': products };
                break;

            case config.BUNDLE_TYPES[2]:
                products = mapOfferVariants(bundle.cross_sell.offerProducts, responses);
                data = { 'cross_sell.offerProducts': products };
                break;
        }

        Bundle.findByIdAndUpdate(bundle.id, data).exec((err, res) => {
            if (err) return reject(err);
            resolve(res);
        });
    });
}

function mapOfferVariants(products, responses) {
    _.map(products, p => {
        if (responses.length) {
            const result = _.find(responses, ['productId', p.id]);
            p.offerVariants = result.offerVariants;

            result.variants.forEach(v => {
                v.image_url = p.image_url;
                let variantImage = _.find(p.images, ['id', v.image_id]);
                if (variantImage) v.image_url = variantImage.src;
            });
            p.variants = Func.searchVariants(result.variants).originVariants;
        } else {
            p.variants = Func.searchVariants(p.variants).originVariants;
        }
        return p;
    });
    return products;
}

function removeVariants(data) {
    return new Promise((resolve, reject) => {
        let promises = [], products = [];

        switch (data.type) {
            case config.BUNDLE_TYPES[0]:
                products = data.products;
                return resolve();
                break;

            case config.BUNDLE_TYPES[1]:
                products = data.offer.products;
                break;

            case config.BUNDLE_TYPES[2]:
                products = data.cross_sell.offerProducts;
                break;
        }

        Func.shopAPI(data.shop).then((shopAPI) => {
            products.forEach(product => promises.push(removeVariantOnProduct(shopAPI, data, product)));
        }, err => reject(err));
        Promise.all(promises).then(res => resolve(res)).catch(err => reject(err));
    });
}

function removeVariantOnProduct(shopAPI, data, product) {
    return new Promise((resolve, reject) => {
        const productId = product.id;
        Func.getVariants(shopAPI, productId).then((variants) => {
            if (_.isEmpty(variants)) return;
            removeCustomVariants(shopAPI, data, productId, variants).then((res) => resolve(res), err => reject(err));
        }, err => reject(err));
    });
}

function removeCustomVariants(shopAPI, data, productId, variants) {
    return new Promise((resolve, reject) => {
        let promises = [];
        const customVariants = Func.searchVariants(variants, data.type).customVariants;
        if (!customVariants.length) {
            return resolve();
        }
        customVariants.forEach(variant => promises.push(Func.removeVariant(shopAPI, productId, variant.id)));
        Promise.all(promises).then(res => resolve(res)).catch(err => reject(err));
    });
}

module.exports = { create, update, remove, status, saveBundleJsonData };