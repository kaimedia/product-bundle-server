function bd_init() {
    bundleProductId = null, bundleDiscountInfo = null, hasCrossSellsItemAdded = false, bundleJsonData = [], bundleOfferSelected = {}, bundleCurrentVariant = null;
    bundleShopDomain = "{{shopDomain}}";
    bundleServerUrl = "{{apiURI}}";
    bundleJsonUrl = "{{bundleData}}?v=" + (new Date().getTime());
    bundleStyleUrl = "{{styleURI}}?v=" + (new Date().getTime());

    if (window.ShopifyAnalytics) {
        if (window.ShopifyAnalytics.meta && window.ShopifyAnalytics.meta.page) {
            if (window.ShopifyAnalytics.meta.page.pageType === 'product') {
                bundleProductId = window.ShopifyAnalytics.meta.page.resourceId;
            }
        }
    }

    $('head').append('<link rel="stylesheet" type="text/css" href="' + bundleStyleUrl + '" />');
}

(function () {

    bd_init();

    var template = bd_GetTemplate();

    if (template === "product") {
        if (!bundleProductId) {
            return;
        }

        bd_MakeRequest("GET", bundleJsonUrl, function (response) {
            var bundleData = JSON.parse(response);
            if (!bundleData.length) return;
            bundleJsonData = bundleData;
            bd_GroupGeneration();
            bd_OfferGeneration();
            bd_SwitchAddToCartButton(0);
        });

        if ($(".product-form__input").length) {
            $(".product-form__input option").each(function () {
                if ($(this).val().indexOf("+") > -1 || $(this).val().indexOf("%") > -1) {
                    $(this).remove();
                }
            });
        }

        $('form[action^="/cart/add"] input[type="submit"], form[action^="/cart/add"] button[type="submit"]').click(function () {
            if (theme.settings.cartType === 'drawer') {
                bd_AjaxCart.init();
                setTimeout(function () {
                    bd_AjaxCart.load();
                }, 1500);
            }
        });

        $(".single-option-selector__radio").click(function () {
            if (Object.keys(bundleOfferSelected).length) {
                setTimeout(function () {

                    var offerVariants,
                        offerVariant,
                        variantEntire,
                        variantSales,
                        newEntire,
                        newVariantEntire,
                        newVariantSales = [];

                    bd_GetOriginVariant();

                    offerVariants = bundleOfferSelected.offerVariants;
                    variantEntire = bundleOfferSelected.variantEntire;
                    variantSales = bundleOfferSelected.variantSales;
                    newEntire = bd_GetVariantByValue(0, offerVariants);
                    offerVariant = bd_GetVariantSale(offerVariants);

                    newVariantEntire = {
                        custom_variant_id: newEntire.custom_variant_id,
                        origin_variant_id: newEntire.origin_variant_id,
                        price: variantEntire.price,
                        value: variantEntire.value,
                        numPieces: variantEntire.numPieces,
                    }

                    variantSales.forEach(function (e) {
                        newVariantSales.push({
                            custom_variant_id: offerVariant.custom_variant_id,
                            origin_variant_id: parseInt(bundleCurrentVariant),
                            free_qty: e.free_qty,
                            origin_price: e.origin_price,
                            price: e.price,
                            value: e.value,
                        });
                    });

                    bundleOfferSelected.variantEntire = newVariantEntire;
                    bundleOfferSelected.variantSales = newVariantSales;

                }, 100);
            }
        });
    }

    if (template === "cart") {
        bd_RenderTotals();
        var checkout_selectors = ["input[name='checkout']", "button[name='checkout']", "[href$='checkout']"];
        checkout_selectors.forEach(function (selector) {
            var els = document.querySelectorAll(selector);
            if (typeof els == "object" && els) {
                for (var i = 0; i < els.length; i++) {
                    var el = els[i];
                    if (typeof el.addEventListener != "function") {
                        return;
                    }
                    el.addEventListener("click", function (e) {
                        e.preventDefault();
                        el.innerHTML = 'Processing... please wait.';
                        bd_OnClickCheckout();
                    }, false);
                }
            }
        });
    }
})(window);

function bd_OnClickCheckout(e) {
    var checkOutUrlParams = {};
    var checkOutUrl = "/checkout";
    if (!bundleDiscountInfo) {
        window.location.href = checkOutUrl;
        return;
    }
    bd_MakeRequest('POST',
        bundleServerUrl + '/assets/generate_discount_code/' + bundleShopDomain,
        bundleDiscountInfo,
        function (data) {
            if (!data) {
                return;
            }
            try {
                data = JSON.parse(data);
                if (data.code) {
                    checkOutUrlParams.discount = data.code;
                }
                if (Object.keys(checkOutUrlParams).length) {
                    checkOutUrl += "?" + bd_QueryString(checkOutUrlParams);
                }
            }
            catch (err) {
                bd_ErrorHanding(err);
            }
            window.location.href = checkOutUrl;
        }
    );
}

function bd_RenderTotals() {
    bd_GetBundleTypes(function (data, cart, items) {
        var groups = data.groups;
        var offers = data.offers;
        var cross_sells = data.cross_sells;
        bd_CalculatePriceItems(cart, items, groups, cross_sells, offers);
        bd_CalculatePriceTotal(cart, groups, cross_sells);
    });
}

function bd_CalculatePriceItems(cart, items, groups, cross_sells, offers) {
    if (
        !offers.length
        && !groups.length
        && !cross_sells.length) {
        bd_ResetCheckoutButton();
    }
    else if (
        offers.length
        && !groups.length
        && !cross_sells.length) {
        bd_OfferRenderCartItem(cart, offers);
    }
    else if (
        offers.length
        && groups.length
        && !cross_sells.lengt) {
        bd_OfferRenderCartItem(cart, offers);
        bd_GroupRenderCartItem(groups);
    }
    else if (
        groups.length
        && cross_sells.length
        && !offers.length) {
        bd_GroupCrossSellRenderCartItem(items, groups, cross_sells);
    }
    else if (
        groups.length &&
        !cross_sells.length
        && !offers.length) {
        bd_GroupRenderCartItem(groups);
    }
    else if (
        cross_sells.length
        && !groups.length
        && !offers.length) {
        bd_CrossSellRenderCartItem(items, cross_sells);
    }
}

function bd_GroupRenderCartItem(groups) {
    groups.forEach(function (bundle) {
        bundle.products.forEach(function (e) {
            bd_RenderItemsPricesHtml(e);
        });
    });
}

function bd_CrossSellRenderCartItem(items, cross_sells) {
    cross_sells.forEach(function (bundle) {
        var offerProducts = bd_OnlyGetProductsInCart(bundle.cross_sell.offerProducts, items);
        offerProducts.forEach(function (e) {
            bd_RenderItemsPricesHtml(e);
        });
    });
}

function bd_OfferRenderCartItem(cart, offers) {
    bd_OfferGetCart(cart, offers, function (cartItems) {
        if (!Object.keys(cartItems).length) {
            bd_ResetCheckoutButton();
            return;
        }
        $.post('/cart/update.js', { updates: cartItems }).always(function () {
            window.location.reload();
        });
    });
}

function bd_RemoveVariantSalesInCart(cartItems) {
    return cartItems.filter(function (e) { return e.title.indexOf("OFF") <= -1 });
}

function bd_GetOfferVariants(offerVariants, variantId) {
    var variantEntire = offerVariants.find(function (e) { return e.custom_variant_id == variantId });
    if (variantEntire) {
        return offerVariants.filter(function (e) { return e.origin_variant_id == variantEntire.origin_variant_id });
    } else {
        return offerVariants.filter(function (e) { return e.origin_variant_id == variantId });
    }
}

function bd_GetTotalQuantityProduct(arr, callback) {
    var total_quantity = 0;
    arr.forEach(function (e) { total_quantity += e.quantity });
    return callback(total_quantity);
}

function bd_OfferGetCart(cart, offers, callback) {
    var originItems,
        rollCart,
        mainItems,
        pushItems = [],
        noteCarts,
        tmp = {};

    // set origin cart items
    originItems = cart.items;

    // remove all variant Sales
    mainItems = bd_RemoveVariantSalesInCart(originItems);

    // gop tong so luong cho moi san pham
    mainItems.forEach(function (e) {
        if (!tmp[e.product_id] || tmp[e.product_id] === undefined) tmp[e.product_id] = [];
        tmp[e.product_id].push(e);
    });

    // duyet qua tung san pham
    for (let proId in tmp) {
        var number_of_variants = tmp[proId].length;

        bd_GetTotalQuantityProduct(tmp[proId], function (total_quantity) {
            if (number_of_variants == 1) {
                tmp[proId].forEach(function (item) {
                    pushItems.concat(bd_AjustOfferCartItems(proId, offers, originItems, item));
                });
            }

            var item = bd_RandomArray(tmp[proId]);
            pushItems = pushItems.concat(bd_AjustOfferCartItems(proId, offers, originItems, item, total_quantity));
        });

    }

    // kiem tra xem co ton tai variant entire nao trong gio hang khong? neu khong thi xoa toan bo variant sales va free
    pushItems = bd_CheckVariantSales(pushItems, originItems);

    // update lai so luong trong gio hang
    bd_ReBuildQuantityCartItems(originItems, pushItems, function (totalItems) {
        bd_UpdateNotesCart(totalItems, function (noteCarts) {
            return callback(noteCarts);
        });
    });
}

function bd_CheckVariantSales(pushItems, originItems) {
    var customItems = originItems.filter(function (e) { return e.title.indexOf("%") !== -1 });
    customItems.forEach(function (e) {
        var hasVariantEntire = originItems.filter(function (o) {
            return o.product_id == e.product_id && o.title.indexOf("off") !== -1 && o.title.indexOf("%") !== -1;
        });
        if (!hasVariantEntire.length) {
            pushItems.push({
                product_id: e.product_id,
                variant_id: e.variant_id,
                quantity: 0,
            });
        }
    });
    return pushItems;
}

function bd_AjustOfferCartItems(productId, offers, originItems, item, total_quantity = null) {
    var pushItems = [];

    // kiem tra co bundle nao bao gom san pham nay khong
    var bundle = bd_OfferGetBundle(offers, productId);
    if (bundle) {
        // product in bundle
        var product = bd_GetProduct(bundle.offer.products, productId);

        // custom variants by variant id
        var offerVariants = bd_GetOfferVariants(product.offerVariants, item.variant_id);

        // get Price Levels tu bundle
        var priceLevels = bd_SortPriceLevels(bundle.offer.price_levels, 'desc');
        var priceLevelLength = priceLevels.length;

        var isReset = true;

        // get variant entire
        var variantEntire = offerVariants.find(function (e) { return e.value == 0 });

        // get variant entire in cart items
        var variantEntireInCart = originItems.find(function (e) { return e.variant_id == variantEntire.custom_variant_id });

        var item_quantity = item.quantity;
        var number_of_products = item_quantity;

        if (total_quantity) {
            number_of_products = total_quantity;

            // var numQty = 0;
            // var mainVariantsInCart = originItems.filter(function (e) { return e.product_id == productId });
            // mainVariantsInCart.forEach(function (e) { numQty += e.quantity });
        }

        // check levels
        for (let i = 0; i < priceLevelLength; i++) {
            var variantEntireQty = false;

            // tong so pack
            var numPieces = priceLevels[i].origin_qty;
            if (priceLevels[i].value != 100) {
                numPieces += priceLevels[i].free_qty;
            }

            if (number_of_products >= numPieces) {

                isReset = false;

                // nhom cac level co cung origin_qty
                var breaked = false;
                var levelGroups = bd_GetPriceLevelsByQty(priceLevels, priceLevels[i].origin_qty);

                levelGroups.forEach(function (level) {
                    var option = offerVariants.find(function (e) { return e.value == level.value });
                    var variantSaleInCart = originItems.find(function (e) { return e.variant_id == option.custom_variant_id });
                    if (variantSaleInCart && variantSaleInCart.quantity == level.free_qty
                        && variantEntireInCart && variantEntireInCart.quantity == item_quantity) {
                        console.log('break');
                        breaked = true;
                        return;
                    } else {
                        // console.log(numQty);
                        // console.log(number_of_products);

                        // if (numQty && numQty === number_of_products) {
                        //     breaked = true;
                        //     return;
                        // }

                        if (bundle.offer.hasSale) {
                            if (item_quantity > numPieces) {
                                variantEntireQty = item_quantity - level.free_qty;
                            } else {
                                variantEntireQty = level.origin_qty;
                            }
                        }
                        pushItems.push({
                            product_id: productId,
                            variant_id: option.custom_variant_id,
                            quantity: level.free_qty,
                        });
                    }
                });

                if (breaked) {
                    break;
                }

                // them Variant Entire vao gio hang
                pushItems.push(
                    {
                        product_id: productId,
                        variant_id: variantEntire.custom_variant_id,
                        quantity: variantEntireQty ? variantEntireQty : item_quantity,
                    },
                    {
                        product_id: productId,
                        variant_id: variantEntire.origin_variant_id,
                        quantity: 0,
                    },
                );

                break;
            }
        }

        // chuyen ve Variant goc
        if (isReset && item.variant_id == variantEntire.custom_variant_id && bundle.offer.hasFree) {
            console.log('ve variant goc');
            pushItems.push(
                {
                    product_id: productId,
                    variant_id: variantEntire.origin_variant_id,
                    quantity: item_quantity,
                },
                {
                    product_id: productId,
                    variant_id: variantEntire.custom_variant_id,
                    quantity: 0,
                },
            );
        }
    }

    return pushItems;
}

function bd_ReBuildQuantityCartItems(originItems, pushItems, callback) {
    var totalItems = [];
    if (pushItems.length) {
        var subItems = [];
        originItems.forEach(function (o) {
            var origin = { quantity: o.quantity, product_id: o.product_id, variant_id: o.variant_id };
            var hasMain = pushItems.find(function (m) { return m.product_id == o.product_id });
            if (hasMain && o.title.indexOf("OFF") !== -1) {
                origin.quantity = 0;
            }
            if (!pushItems.find(function (e) { return e.variant_id == origin.variant_id })) {
                subItems.push(origin);
            }
        });
        totalItems = subItems.concat(pushItems);
    }
    return callback(totalItems);
}

function bd_UpdateNotesCart(array, callback) {
    var noteCarts = {};
    array.forEach(function (e) {
        noteCarts[e.variant_id] = e.quantity;
    });
    return callback(noteCarts);
}

function bd_RandomArray(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
}

function bd_RenderItemsPricesHtml(product) {
    var oldPrice = product.line_price;
    var newPrice = (Number(product.new_price) * product.line_quantity).toFixed(2);
    if ($(".cart__row__" + product.id + ".cart__row__" + product.variant_id).length) {
        $(".cart__row__" + product.id + ".cart__row__" + product.variant_id).find(".cart__price__item .lion-cart-item-line-price .money").html('$' + newPrice + ' USD');
    }
    bd_ResetCheckoutButton();
}

function bd_GetBundleTypes(callback) {
    var cartItems = [];
    $.get('/cart.js').always(function (res) {
        if (res && res.status == 200) {
            var cart = JSON.parse(res.responseText);

            if (!cart.items.length) {
                return;
            }

            cart.items.forEach(function (e) {
                cartItems.push({
                    id: e.product_id,
                    variant_id: e.id,
                    variant_title: e.variant_title,
                    line_quantity: e.quantity,
                    line_price: e.line_price / 100,
                    original_price: e.original_price / 100,
                });
            });

            bd_MakeRequest("GET", bundleJsonUrl, function (response) {
                var bundleData = response;
                if (!bundleData) {
                    bd_ResetCheckoutButton();
                    return;
                }
                try {
                    bundleData = JSON.parse(response);
                    bundleJsonData = bundleData;
                    var bundleGroups = bd_GetBundleTypeGroup(bundleData, cartItems);
                    var bundleOffers = bd_GetBundleTypeOffers(bundleData, cartItems);
                    var bundleCrossSells = bd_GetBundleTypeCrossSells(bundleData, cartItems);
                    return callback({
                        groups: bundleGroups,
                        offers: bundleOffers,
                        cross_sells: bundleCrossSells,
                    }, cart, cartItems);
                }
                catch (err) {
                    bd_ErrorHanding(err);
                }
            });
        }
    });
}

function bd_GetBundleTypeGroup(bundleData, items) {
    var bundleArr = [],
        bundleResults = [];

    items.forEach(function (e) {
        var bundle = bd_GroupGetBundle(bundleData, e.id);
        if (bundle) bundleArr.push(r);
    });
    if (!bundleArr.length) {
        return [];
    }
    bundleArr = bd_UniqueArray(bundleArr);
    bundleArr.forEach(function (bundle) {
        var prodArr = [],
            bundleQuantity = 0,
            bundlePrice = 0;

        prodArr = bd_OnlyGetProductsInCart(bundle.products, items);
        bundle.products = prodArr;
        prodArr.forEach(function (e) {
            bundleQuantity += e.line_quantity;
            bundlePrice += e.line_price;
        });
        bundlePrice = bundlePrice.toFixed(2);
        if (bundleQuantity >= Object.keys(bundle.products).length && bundlePrice >= bundle.old_total) {
            bundleResults.push(bundle);
        }
    });
    return bundleResults;
}

function bd_GetBundleTypeCrossSells(bundleData, items) {
    var bundleArr = [],
        getCrossSellss = [],
        fullCrossSells = [],
        partialCrossSells = [];

    items.forEach(function (e) {
        var bundle = bd_CrossSellGetBundleByTriggerProduct(bundleData, e.id);
        if (bundle) bundleArr.push(r);
    });
    if (!bundleArr.length) {
        return [];
    }
    getCrossSellss = bd_GetCrossSellsExists(bundleArr, items);
    fullCrossSells = getCrossSellss.fullCrossSellsProducts;
    partialCrossSells = getCrossSellss.partialCrossSellsProducts;
    if (partialCrossSells.length) {
        bd_ShowCrossSellsPopup(partialCrossSells);
    }
    if (fullCrossSells.length) {
        fullCrossSells.forEach(function (bundle) {
            bundle.cross_sell.offerProducts = bd_AssignLineQty(bundle.cross_sell.offerProducts, items);
            return bundle;
        });
    }
    return fullCrossSells;
}

function bd_GetBundleTypeOffers(bundleData, items) {
    var bundleArr = [];
    items.forEach(function (e) {
        var r = bd_OfferGetBundle(bundleData, e.id);
        if (r) bundleArr.push(r);
    });
    if (!bundleArr.length) {
        return [];
    }
    bundleArr = bd_UniqueArray(bundleArr);
    return bundleArr;
}

function bd_CalculatePriceTotal(cart, groups, cross_sells) {
    var subTotal = 0, originalTotal = cart.original_total_price / 100;
    if (groups.length && cross_sells.length) {
        subTotal = bd_AllSubTotal(groups, cross_sells, cart);
        bd_RenderSubTotalHtml(originalTotal, subTotal);
    } else if (groups.length && !cross_sells.length) {
        subTotal = bd_GroupSubTotal(groups, cart);
        bd_RenderSubTotalHtml(originalTotal, subTotal);
    } else if (!groups.length && cross_sells.length) {
        subTotal = bd_CrossSellSubTotal(cross_sells, cart);
        bd_RenderSubTotalHtml(originalTotal, subTotal);
    }
}

function bd_AllSubTotal(groups, cross_sells, cart) {
    var subTotal = 0;
    cart.items.forEach(function (e) {
        var sub = e.price / 100;
        var bundleGroup = bd_GroupGetBundle(groups, e.product_id);
        var bundleCrossSell = bd_CrossSellGetBundleByOfferProduct(cross_sells, e.product_id);
        if (bundleGroup && bundleCrossSell) {
            var percent = bundleGroup.percent;
            if (bundleGroup.discount_method === "fixed_amount") {
                var new_price = (sub - bundleGroup.fixed_price).toFixed(2);
                new_price = new_price > 0 ? new_price : 0;
                percent = (100 - ((new_price / sub) * 100)).toFixed(2);
            }
            percent = (parseFloat(percent) + parseFloat(bundleCrossSell.cross_sell.discountValue)) / 100;
            sub = (sub - (sub * percent)) * e.quantity;
            subTotal += parseFloat(sub.toFixed(2));
        } else if (bundleGroup && !bundleCrossSell) {
            if (bundleGroup.discount_method === "percentage") {
                sub = (sub - (sub * bundleGroup.percent) / 100) * e.quantity;
                subTotal += parseFloat(sub.toFixed(2));
            } else if (bundleGroup.discount_method === "fixed_amount") {
                sub = sub * e.quantity;
                subTotal += parseFloat(sub.toFixed(2));
                subTotal = subTotal - bundleGroup.fixed_price;
                subTotal = subTotal > 0 ? subTotal : 0;
            }
        } else if (!bundleGroup && bundleCrossSell) {
            sub = (sub - (sub * bundleCrossSell.cross_sell.discountValue) / 100) * e.quantity;
            subTotal += parseFloat(sub.toFixed(2));
        } else {
            sub = sub * e.quantity;
            subTotal += parseFloat(sub.toFixed(2));
        }
    });
    return subTotal.toFixed(2);
}

function bd_GroupSubTotal(groups, cart) {
    var subTotal = 0;
    cart.items.forEach(function (e) {
        var sub = e.price / 100;
        var bundle = bd_GroupGetBundle(groups, e.product_id);
        if (bundle) {
            if (bundle.discount_method === "percentage") {
                sub = (sub - (sub * bundle.percent) / 100) * e.quantity;
                subTotal += parseFloat(sub.toFixed(2));
            } else if (bundle.discount_method === "fixed_amount") {
                sub = sub * e.quantity;
                subTotal += parseFloat(sub.toFixed(2));
                subTotal = subTotal - bundle.fixed_price;
                subTotal = subTotal > 0 ? subTotal : 0;
            }
        } else {
            sub = sub * e.quantity;
            subTotal += parseFloat(sub.toFixed(2));
        }
    });
    return subTotal.toFixed(2);
}

function bd_CrossSellSubTotal(cross_sells, cart) {
    var subTotal = 0;
    cart.items.forEach(function (e) {
        var sub = e.price / 100;
        var bundle = bd_CrossSellGetBundleByOfferProduct(cross_sells, e.product_id);
        if (bundle) {
            sub = (sub - (sub * bundle.cross_sell.discountValue) / 100) * e.quantity;
        } else {
            sub = sub * e.quantity;
        }
        subTotal += parseFloat(sub.toFixed(2));
    });
    return subTotal.toFixed(2);
}

function bd_RenderSubTotalHtml(originalTotal, subTotal) {
    var savingTotal = (originalTotal - subTotal).toFixed(2);
    if ($(".cart__subtotal").length) {
        $(".cart__subtotal .bundle-line-discount").html("$" + subTotal + " USD");
    }
    bundleDiscountInfo = { originalTotal, subTotal, savingTotal };
    bd_ResetCheckoutButton();
}

function bd_GroupCrossSellRenderCartItem(items, groups, cross_sells) {
    var allProducts = [],
        newProducts = [];

    var productsInGroups = bd_GetProductsFromBundleGroups(groups);
    var productsInCrossSellss = bd_GetProductsFromBundleCrossSells(cross_sells, items);
    allProducts = productsInGroups.concat(productsInCrossSellss);

    allProducts.forEach(function (p) {
        var product = bd_GetProduct(newProducts, p.id);
        if (product) {
            p.percent += product.percent;
            p.new_price = Number((p.old_price - (p.old_price * (p.percent / 100))).toFixed(2));
            newProducts = newProducts.filter(function (e) { return e.id !== p.id });
        }
        newProducts.push(p);
    });

    newProducts.forEach(function (e) {
        bd_RenderItemsPricesHtml(e);
    });
}

function bd_GetProductsFromBundleGroups(bundles) {
    var productArr = [];
    bundles.forEach(function (bundle) {
        bundle.products.forEach(function (p) {
            var percent = bundle.percent;
            if (bundle.discount_method == 'fixed_amount') {
                percent = (100 - ((p.new_price / p.old_price) * 100)).toFixed(2);
            }
            productArr.push({
                id: p.id,
                title: p.title,
                line_price: p.line_price,
                line_quantity: p.line_quantity,
                old_price: p.old_price,
                new_price: p.new_price,
                percent: Number(percent),
            });
        });
    });
    return productArr;
}

function bd_GetProductsFromBundleCrossSells(cross_sells, items) {
    var productArr = [];
    cross_sells.forEach(function (bundle) {
        var offerProducts = bd_OnlyGetProductsInCart(bundle.cross_sell.offerProducts, items);
        offerProducts.forEach(function (p) {
            productArr.push({
                id: p.id,
                title: p.title,
                line_price: p.line_price,
                line_quantity: p.line_quantity,
                old_price: p.old_price,
                new_price: p.new_price,
                percent: bundle.cross_sell.discountValue,
            });
        });
    });
    return productArr;
}

function bd_GetCrossSellsExists(bundleArr, items) {
    var fullCrossSellsProducts = [],
        partialCrossSellsProducts = [];

    bundleArr = bd_UniqueArray(bundleArr);
    items.forEach(function (e) {
        var bundle = bd_CrossSellGetBundleByOfferProduct(bundleArr, e.id);
        if (bundle) {
            fullCrossSellsProducts.push(bundle);
            bundleArr = bundleArr.filter(function (obj) { return obj._id != bundle._id });
        }
    });

    return {
        fullCrossSellsProducts,
        partialCrossSellsProducts: bundleArr,
    };
}

function bd_ShowCrossSellsPopup(partialGroups) {
    if (!bd_GetCookie("__bundleCrossSellClosed")) {
        setTimeout(function () {
            bd_CrossSellPopupHtml(partialGroups);
        }, 2000);
    }
}

function bd_CrossSellPopupHtml(groups) {
    var html = '';
    html += '<div id="bundlePopupCrossSellss" class="jquery-dpModal current blocker">';
    html += '<div id="dpModal-container" class="dp-popup dp-wrap dp-whModal dp-popup-dpModal">';
    groups.forEach(function (bundle) {
        var countProducts = bundle.cross_sell.offerProducts.length;
        html += '<div class="multiple-products-true ba-product-bundle clearfix">';
        html += '<h3 class="upsell-title">Buy ' + countProducts + ' for ' + bundle.cross_sell.discountValue + '% off</h3>';
        bundle.cross_sell.offerProducts.forEach(function (p) {
            var variants = bd_RemoveAllCustomVariants(p.variants);
            var multi_variants_class = (variants.length > 1) ? 'multiple-variants' : '';
            html += '<div class="product-container discount-applies-true" data-product-id="' + p.id + '" data-variant-id="' + variants[0].id + '">';
            html += '<div class="image">';
            html += '<a href="/products/' + p.handle + '" target="_blank">';
            html += '<img src="' + p.image_url + '">';
            html += '</a>';
            html += '</div>';
            html += '<div class="details ' + multi_variants_class + '">';
            html += '<div class="product-title">';
            html += '<a href="/products/' + p.handle + '" target="_blank">';
            html += p.title;
            html += '</a>';
            html += '</div>';
            html += '<div class="product-price">';
            html += '<span class="ba-sale price">$' + p.new_price.toFixed(2) + ' <s class="price">$' + p.old_price + '</s></span>';
            html += '</div>';
            html += '<div class="product-quantity">';
            html += '<input type="number" class="" value="1" min="1" max="999">';
            html += '</div>';
            html += '</div>';
            html += '<div class="actions">';
            if (variants.length > 1) {
                html += '<select>';
                variants.forEach(function (v) {
                    html += '<option value="' + v.id + '" data-price="' + v.price + '" data-compare-at-price="' + v.compare_at_price + '">' + v.title + '</option>';
                });
                html += '</select>';
            }
            html += '<button class="add-cross_sells" onclick="bd_AddCrossSellss(this)">Add to cart</button>';
            html += '</div>';
            html += '</div>';
        });
        html += '</div>';
    });
    html += '<div class="no-thanks">';
    html += '<button class="btn btn-success btnCompleteCrossSells" style="display:none" onclick="bd_CompleteCrossSells()">OK</button>';
    html += '<a href="javascript:void(0)" onclick="bd_CloseCrossSellsPopup(this, true)">No thanks</a>';
    html += '</div>';
    html += '<a href="javascript:void(0)" class="close-dpModal" onclick="bd_CloseCrossSellsPopup(this)">Close</a></div>';
    html += '</div>';
    $('body').append(html);
}

function bd_RemoveAllCustomVariants(variants) {
    return variants.filter(function (e) { return e.title.indexOf("+") <= -1 && e.title.indexOf("%") <= -1 });
}

function bd_AddCrossSellss(obj) {
    var $this = $(obj);
    var $container = $this.closest('.product-container');
    var variant_id = $container.data('variant-id');
    var quantity = $container.find('.product-quantity input[type="number"]').val();
    var variants = $this.prev('select');
    if (variants.length) variant_id = variants.val();
    $.post('/cart/add.js', { id: variant_id, quantity: quantity }).always(function (res) {
        if (res && res.status == 200) {
            $this.addClass('disabled').attr('disabled', true).text('Item added');
            $this.closest('#dpModal-container').find('.btnCompleteCrossSells').show();
            hasCrossSellsItemAdded = true;
            return false;
        }
    });
}

function bd_CloseCrossSellsPopup(obj, hasCookie = false) {
    var $this = $(obj);
    $this.closest('#bundlePopupCrossSellss').removeClass('blocker');
    if (hasCookie) {
        var exdays = new Date();
        exdays.setTime(exdays.getTime() + (1 * 60 * 60 * 1000));
        bd_SetCookie("__bundleCrossSellClosed", 1, exdays);
    }
    if (hasCrossSellsItemAdded) {
        bd_CompleteCrossSells();
    }
}

function bd_CompleteCrossSells() {
    window.location.reload();
}

function bd_AssignLineQty(productArr1, productArr2) {
    return productArr1.map(function (a) { return Object.assign(a, productArr2.find(function (b) { return b.id == a.id })) });
}

function bd_OnlyGetProductsInCart(bundleProducts, items) {
    var results = [];
    items.forEach(function (i) {
        if (bd_GetProduct(bundleProducts, i.id)) {
            if (!i.variant_title || (i.variant_title && i.variant_title.indexOf("OFF") <= -1)) {
                results.push(i);
            }
        }
    });
    return bd_AssignLineQty(results, bundleProducts);
}

function bd_UniqueArray(array) {
    return Array.from(new Set(array));
}

function bd_GetProduct(products, productId) {
    return products.find(function (e) { return e.id == productId });
}

function bd_GetVariantByValue(value, offerVariants) {
    return offerVariants.find(function (e) {
        if (bundleCurrentVariant) {
            return e.value == value && e.origin_variant_id == bundleCurrentVariant;
        } else {
            return e.value == value;
        }
    });
}

function bd_OfferGetBundle(data, productId) {
    return data.find(function (e) { return e.type === 'offer' && e.offer.productIds.indexOf(productId) !== -1 });
}

function bd_GroupGetBundle(data, productId) {
    return data.find(function (e) { return e.type === 'group' && e.productIds.indexOf(productId) !== -1 });
}

function bd_CrossSellGetBundleByOfferProduct(data, productId) {
    return data.find(function (e) { return e.type === 'cross_sell' && e.cross_sell.offerProductIds.indexOf(productId) !== -1 });
}

function bd_CrossSellGetBundleByTriggerProduct(data, productId) {
    return data.find(function (e) { return e.type === 'cross_sell' && e.cross_sell.triggerProductIds.indexOf(productId) !== -1 });
}

function bd_GetVariantEntire(offerVariants) {
    return offerVariants.find(function (e) {
        if (bundleCurrentVariant) {
            return e.value == 0 && e.origin_variant_id == bundleCurrentVariant;
        } else {
            return e.value == 0;
        }
    });
}

function bd_GetVariantEntires(offerVariants) {
    return offerVariants.filter(function (e) { return e.value == 0 });
}

function bd_GetVariantSale(offerVariants) {
    return offerVariants.find(function (e) {
        if (bundleCurrentVariant) {
            return e.value != 0 && e.origin_variant_id == bundleCurrentVariant;
        } else {
            return e.value != 0;
        }
    });
}

function bd_SortPriceLevels(priceLevels, order = 'asc') {
    switch (order) {
        case 'asc':
            return priceLevels.sort(function (a, b) { return a.origin_qty - b.origin_qty });
            break;
        case 'desc':
            return priceLevels.sort(function (a, b) { return b.origin_qty - a.origin_qty });
            break;
    }
}

function bd_GetPriceLevelsByQty(priceLevels, originQty) {
    return priceLevels.filter(function (e) { return e.origin_qty == originQty });
}

function bd_OfferGeneration() {
    if (!bundleJsonData.length) {
        return;
    }

    var bundle = bd_OfferGetBundle(bundleJsonData, bundleProductId);
    if (!bundle || typeof bundle !== 'object') {
        return;
    }

    var product = bd_GetProduct(bundle.offer.products, bundleProductId);
    if (!product) {
        return;
    }

    var variantEntire = bd_GetVariantEntire(product.offerVariants);
    var priceLevels = bd_SortPriceLevels(bundle.offer.price_levels, 'asc');

    var renderHTML = '<div class="flw buy-x-get-y-free">';
    renderHTML += '<a href="javascript:void(0)" onclick="bd_SelectOffer(this, 0)" class="active">1 PIECE</a>';

    priceLevels.forEach(function (level) {
        var freeHTML = '',
            pieceHTML = '',
            priceHTML = '',
            numPieces = level.origin_qty,
            pricePack = variantEntire.price,
            variant = bd_GetVariantByValue(level.value, product.offerVariants);

        pricePack = pricePack * level.origin_qty;
        pricePack += variant.price * level.free_qty;

        if (level.value != 100) {
            numPieces += level.free_qty;
        }

        if (level.value == 100) {
            freeHTML = ` + ${level.free_qty} FREE`;
        } else {
            priceHTML = `- <span class="money">$${pricePack.toFixed(2)}</span>`;
        }

        if (numPieces == 1) {
            pieceHTML = `${numPieces} PIECE`;
        } else {
            pieceHTML = `${numPieces} PIECES`;
        }

        renderHTML += '<a href="javascript:void(0)" onclick="bd_SelectOffer(this, ' + level.origin_qty + ')">' + pieceHTML + freeHTML + priceHTML + '</a>';
    });

    renderHTML += '</div>';

    if ($(".bundleOfferDisplayArea").length) {
        $(".bundleOfferDisplayArea").html(renderHTML);
    } else {
        $(".product-single form").prepend(renderHTML);
    }
}

function bd_combinePriceLevels(priceLevels) {
    var tmp = [];
    priceLevels.forEach(function (e) {
        if (!tmp[e.origin_qty] || tmp[e.origin_qty] === undefined) tmp[e.origin_qty] = [];
        tmp[e.origin_qty].push(e);
    });
    return tmp;
}

function bd_SelectOffer(obj, origin_qty, num_pack = null) {
    $(".product-single__quantity").hide();
    $(".product-form__item").addClass("width-full");
    $(".btn-cart-bundle-offer").remove();

    if ($(obj).hasClass('active')) {
        $(obj).removeClass('active');
        $(".product-single__quantity").show();
        bd_SwitchAddToCartButton(0);
    } else {
        $(obj).closest('.buy-x-get-y-free').find('a').removeClass('active');
        $(obj).addClass('active');
        bd_SwitchAddToCartButton(origin_qty);
    }
}

function bd_SwitchAddToCartButton(originQty) {
    var customVariant = bd_GetCustomVariantOnSelect(originQty);
    if (!customVariant) {
        return;
    }

    bundleOfferSelected = {
        quantity: originQty,
        offerVariants: customVariant.offerVariants,
        variantEntires: customVariant.variantEntires,
        variantEntire: customVariant.variantEntire,
        variantSales: customVariant.variantSales,
        hasSale: customVariant.hasSale,
        hasFree: customVariant.hasFree,
    };

    $(".btn--add-to-cart").hide();
    var btnAddCart = $(".btn--add-to-cart").clone();
    btnAddCart.attr('type', 'button');
    btnAddCart.attr('onclick', 'bd_AddToCartByOffer()');
    btnAddCart.addClass('btn-cart-bundle-offer');
    btnAddCart.show();
    btnAddCart.insertAfter($(".btn--add-to-cart"));
}

function bd_GetCustomVariantOnSelect(originQty) {
    if (!bundleJsonData.length)
        return false;

    var bundle = bd_OfferGetBundle(bundleJsonData, bundleProductId);
    if (!bundle)
        return false;

    var product = bd_GetProduct(bundle.offer.products, bundleProductId);
    if (!product)
        return false;

    var hasFree = bundle.offer.hasFree;
    var hasSale = bundle.offer.hasSale;

    bd_GetOriginVariant();
    var variantEntires = bd_GetVariantEntires(product.offerVariants);
    var variantEntire = bd_GetVariantEntire(product.offerVariants);
    var variantSales = [];

    bundle.offer.price_levels.forEach(function (level) {
        var variant = bd_GetVariantByValue(level.value, product.offerVariants);
        if (level.origin_qty == originQty) {
            variant.free_qty = level.free_qty;
        } else {
            if (variant.value !== 100) {
                variant.free_qty = 0;
            }
        }

        var numPieces = originQty;
        if (level.value !== 100) {
            numPieces += level.free_qty;
        }
        variantEntire.numPieces = numPieces;

        variantSales.push(variant);
    });

    console.log(variantEntire);

    return { offerVariants: product.offerVariants, variantEntires, variantEntire, variantSales, hasSale, hasFree };
}

function bd_AddToCartByOffer() {
    var noteCarts = {};

    if (Object.keys(bundleOfferSelected).length) {
        // bundleOfferSelected.variantEntires.forEach(function (e) {
        //     noteCarts[e.origin_variant_id] = 0;
        // });

        // bundleOfferSelected.offerVariants.forEach(function (e) {
        //     noteCarts[e.custom_variant_id] = 0;
        // });

        if (bundleOfferSelected.quantity == 0) {
            console.log('1 piece');
            noteCarts[bundleOfferSelected.variantEntire.origin_variant_id] = 1;
            noteCarts[bundleOfferSelected.variantEntire.custom_variant_id] = 0;
            // bundleOfferSelected.variantEntires.forEach(function (e) {
            //     noteCarts[e.custom_variant_id] = 0;
            // });
        } else {
            console.log('>= 2 pieces');
            // noteCarts[bundleOfferSelected.variantEntire.origin_variant_id] = 0;
            bundleOfferSelected.variantEntires.forEach(function (e) {
                noteCarts[e.origin_variant_id] = 0;
            });

            noteCarts[bundleOfferSelected.variantEntire.custom_variant_id] = bundleOfferSelected.variantEntire.numPieces;
            // bundleOfferSelected.variantSales.forEach(function (e) {
            //     noteCarts[e.custom_variant_id] = e.free_qty;
            // });
        }
    }

    // console.log(bundleOfferSelected);
    console.log(noteCarts);
    // return;

    if (Object.keys(noteCarts).length) {
        $.post('/cart/update.js', { updates: noteCarts }).always(function () {
            if (theme.settings.cartType === 'drawer') {
                bd_AjaxCart.init();
                bd_AjaxCart.load();
                timber.RightDrawer.open();
            }
        });
    }

}

function bd_GetOriginVariant() {
    if ($("#ProductSelect").length) {
        bundleCurrentVariant = $("#ProductSelect").val();
    }
}

function bd_GroupGeneration() {
    if (!bundleJsonData.length) {
        return;
    }

    var bundle = bd_GroupGetBundle(bundleJsonData, bundleProductId);
    if (!bundle || typeof bundle !== 'object') {
        return;
    }
    var html = "<div id='product_bundles' class='bold-bundles-widget basic-bundle'>";
    html += '<div class="bold-bundles-widget-header"><h3 class="bold-bundles-widget-header__title"></h3></div>';
    html += "<div class='bold-bundles-widget__items'>";
    bundle.products.forEach(function (product) {
        html += "<div class='bold-bundles-widget-item bold-bundles-widget-item--product'>";
        html += "<div class='bold-bundles-widget-item__wrapper'>";
        html += "<div class='bold-bundles-widget-item__thumbnail'>";
        html += "<a class='bold-bundles-widget-item__link' href='/products/" + product.handle + "' target='_blank'><img class='bold-bundles-widget-item__image' src='" + product.image_url + "'></a>";
        html += "</div>";
        html += "<div class='bold-bundles-widget-item__info'>";
        html += "<h4 class='bold-bundles-widget-item__title'>" + product.title + " x " + product.product_quantity + "</h4>";

        if (product.variants) {
            var hidden = 'hidden';
            // if (product.variants[0].title === "Default Title") {
            //     hidden = 'hidden';
            // }
            html += "<select class='bold-bundles-widget-item__variants " + hidden + "' onchange='bd_ChangeVariantOption(this)'>";
            product.variants.forEach(function (variant) {
                html += '<option class="bold-bundles-widget-item__variant" value="' + variant.id + '" data-original-price="' + variant.price + '" data-price="' + variant.bundle_price + '" data-quantity="' + product.product_quantity + '">' + variant.title + '</option>';
            });
            html += "</select>";

            html += "<div class='bold-bundles-widget-item__price'>";
            html += "<span class='bold-bundles-widget-item__price--old money'>$" + product.variants[0].price + " USD</span>&nbsp;";
            html += "<span class='bold-bundles-widget-item__price--new money'>$" + product.variants[0].bundle_price + " USD</span>";
            html += "</div>";
        }

        html += "</div>";
        html += "</div>";
        html += "</div>";
        html += '<div class="bold-bundles-widget-item bold-bundles-widget-item--with-separator"><div class="bold-bundles-widget-item-separator__wrapper"> <span class="bold-bundles-widget-item-separator"><svg class="bold-bundles-widget-item-separator__icon"><use xlink:href="#bold-plus-sign"><svg id="bold-plus-sign" viewBox="0 0 472 472" width="100%" height="100%"><path d="M472,185H287V0H185v185H0v102h185v185h102V287h185V185z"></path></svg></use></svg></span></div></div>';
    });
    html += "</div>";
    html += "<div class='bold-bundles-widget__footer'>";
    html += '<button class="btn bold-bundles-widget__button" type="button" onclick="bd_ClickBundleToCart(this)"><span class="bold-bundles-widget__button--top">Add Bundle to Cart</span></button>';
    html += "</div>";
    html += "</div>";

    if ($(".bundleDisplayArea").length) {
        $(".bundleDisplayArea").html(html);
    } else {
        $(".product-single form").append(html);
    }
}

function bd_ChangeVariantOption(obj) {
    var oldPrice = $(obj).find(':selected').data('original-price');
    var newPrice = $(obj).find(':selected').data('price');
    $(obj).next('.bold-bundles-widget-item__price').find('.bold-bundles-widget-item__price--old').html("$" + oldPrice + " USD");
    $(obj).next('.bold-bundles-widget-item__price').find('.bold-bundles-widget-item__price--new').html("$" + newPrice + " USD");
}

function bd_ResetCheckoutButton() {
    $(".group-button-cart span.cart__checkout").hide();
    $(".group-button-cart button.cart__checkout").remove();
    $(".group-button-cart").prepend('<button type="submit" name="checkout" onclick="bd_OnClickCheckout()" class="btn--secondary cart__checkout">Enter shipping address</button>');
}

function bd_AjaxCartResetCheckoutButton() {
    $(".ajaxcart__footer span.cart__checkout").hide();
    $(".ajaxcart__footer").append('<button type="submit" name="checkout" class="btn--secondary btn--full cart__checkout" onclick="bd_OnClickCheckout()">Enter shipping address <span class="icon icon-arrow-right" aria-hidden="true"></span></button>')
}

function bd_ClickBundleToCart(obj) {
    var cartItems = [], timeOut = 100;
    $(obj).parents("#product_bundles").find(".bold-bundles-widget-item__variants").each(function () {
        cartItems.push({
            id: parseInt($(this).val()),
            quantity: parseInt($(this).find(':selected').data('quantity')),
        });
    });
    cartItems.forEach(function (e) {
        setTimeout(function () {
            $.post('/cart/add.js', e);
        }, timeOut);
        timeOut += 500;
    });
    setTimeout(function () {
        window.location.href = "/cart";
    }, timeOut);
}

function bd_MakeRequest(method, endpoint, data, callback) {
    var xmlhttp = new XMLHttpRequest();
    if (bd_DetectHttps(endpoint) && !bd_DetectServerURI(endpoint)) {
        xmlhttp.withCredentials = true;
    }
    if (method === "GET") {
        if (typeof data === 'function') {
            callback = data;
            data = null;
        } else {
            endpoint += '?' + bd_QueryString(data);
            data = null;
        }
    }
    xmlhttp.open(method, endpoint, true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status === 200) {
            callback(xmlhttp.responseText);
        }
    }
    xmlhttp.send(JSON.stringify(data));
}

function bd_QueryString(obj) {
    var str = [];
    for (var p in obj) {
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    }
    return str.join("&");
}

function bd_DetectHttps(url) {
    if (url.indexOf("https://") == 0) {
        return true;
    }
    return false;
}

function bd_DetectServerURI(url) {
    if (url.indexOf(bundleServerUrl) == 0) {
        return true;
    }
    return false;
}

function bd_SetCookie(cname, cvalue, exdays) {
    var expires = "expires=" + exdays;
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function bd_GetCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function bd_GetTemplate() {
    var classTemplate = $('body').attr('class');
    classTemplate = classTemplate.split('-');
    return classTemplate[1];
}

function bd_ErrorHanding(err) {
    console.log(err);
    bd_ResetCheckoutButton();
    return;
}

var bd_AjaxCart = (function (module, $) {
    'use strict';

    // Public functions
    var init, loadCart;

    // Private general variables
    var settings, isUpdating, $body;

    // Private plugin variables
    var $formContainer,
        $addToCart,
        $cartCountSelector,
        $cartCostSelector,
        $cartContainer;

    // Private functions
    var updateCountPrice,
        formOverride,
        itemAddedCallback,
        itemErrorCallback,
        cartUpdateCallback,
        buildCart,
        cartCallback,
        adjustCart,
        adjustCartCallback,
        qtySelectors,
        validateQty,
        bundleBuildCart,
        subStringUrl,
        subStringVariantTitle;

    /*============================================================================
      Initialise the plugin and define global options
      ==============================================================================*/
    init = function (options) {
        // Default settings
        settings = {
            formSelector: 'form[action^="/cart/add"]',
            cartContainer: '#CartContainer',
            addToCartSelector: 'input[type="submit"]',
            cartCountSelector: '.cart-item-count, .cart-count',
            cartCostSelector: null,
            moneyFormat: '${{amount}}',
            disableAjaxCart: false,
            enableQtySelectors: true
        };

        // Override defaults with arguments
        $.extend(settings, options);

        // Select DOM elements
        $formContainer = $(settings.formSelector);
        $cartContainer = $(settings.cartContainer);
        $addToCart = $formContainer.find(settings.addToCartSelector);
        $cartCountSelector = $(settings.cartCountSelector);
        $cartCostSelector = $(settings.cartCostSelector);

        // General Selectors
        $body = $('body');

        // Track cart activity status
        isUpdating = false;

        // Setup ajax quantity selectors on the any template if enableQtySelectors is true
        if (settings.enableQtySelectors) {
            qtySelectors();
        }

        // Take over the add to cart form submit action if ajax enabled
        if (!settings.disableAjaxCart && $addToCart.length) {
            formOverride();
        }

        // Run this function in case we're using the quantity selector outside of the cart
        adjustCart();
    };

    loadCart = function () {
        $body.addClass('drawer--is-loading');
        ShopifyAPI.getCart(cartUpdateCallback);
    };

    updateCountPrice = function (cart) {
        if ($cartCountSelector) {
            $cartCountSelector.html(cart.item_count).removeClass('hidden-count');

            if (cart.item_count === 0) {
                $cartCountSelector.addClass('hidden-count');
            }
        }
        if ($cartCostSelector) {
            $cartCostSelector.html(
                theme.Currency.formatMoney(cart.total_price, settings.moneyFormat)
            );
        }
    };

    formOverride = function () {
        $formContainer.on('submit', function (evt) {
            evt.preventDefault();

            // Add class to be styled if desired
            $addToCart.removeClass('is-added').addClass('is-adding');

            // Remove any previous quantity errors
            $('.qty-error').remove();

            ShopifyAPI.addItemFromForm(
                evt.target,
                itemAddedCallback,
                itemErrorCallback
            );
        });
    };

    itemAddedCallback = function () {
        $addToCart.removeClass('is-adding').addClass('is-added');

        ShopifyAPI.getCart(cartUpdateCallback);
    };

    itemErrorCallback = function (XMLHttpRequest) {
        var data = eval('(' + XMLHttpRequest.responseText + ')');
        $addToCart.removeClass('is-adding is-added');

        if (data.message) {
            if (data.status === 422) {
                $formContainer.after(
                    '<div class="errors qty-error">' + data.description + '</div>'
                );
            }
        }
    };

    cartUpdateCallback = function (cart) {
        // Update quantity and price
        // updateCountPrice(cart);
        bundleBuildCart(cart);
    };

    subStringUrl = function (url) {
        if (url.lastIndexOf("?variant=") > -1) {
            url = url.slice(0, url.lastIndexOf("?variant="));
        }
        return url;
    }

    subStringVariantTitle = function (title) {
        if (title && title.indexOf("%") !== -1 && title.indexOf("off") !== -1) {
            title = title.split('-');
            title = title[0];
        }
        return title;
    }

    bundleBuildCart = function (cart) {
        var items = [];

        if (!cart.items.length) {
            return buildCart(cart);
        }

        cart.items.forEach(function (e) {
            items.push({
                id: e.product_id,
                variant_id: e.id,
                variant_title: e.variant_title,
            });
        });

        var offers = bd_GetBundleTypeOffers(bundleJsonData, items);
        if (!offers.length) {
            return buildCart(cart);
        }

        bd_OfferGetCart(cart, offers, function (cartItems) {
            if (!Object.keys(cartItems).length) {
                return buildCart(cart);
            }
            $.post('/cart/update.js', { updates: cartItems }).always(function (res) {
                var newCart = JSON.parse(res.responseText);
                updateCountPrice(newCart);
                buildCart(newCart);
            });
        });

    };

    buildCart = function (cart) {
        // Start with a fresh cart div
        $cartContainer.empty();

        // Show empty cart
        if (cart.item_count === 0) {
            $cartContainer.append(
                '<p class="cart--empty-message">' +
                theme.strings.cartEmpty +
                '</p>\n' +
                '<p class="cookie-message">' +
                theme.strings.cartCookies +
                '</p>'
            );
            cartCallback(cart);
            return;
        }

        // Handlebars.js cart layout
        var items = [],
            item = {},
            data = {},
            source = $('#CartTemplate').html(),
            template = Handlebars.compile(source);

        $.each(cart.items, function (index, cartItem) {
            var prodImg;
            if (cartItem.image !== null) {
                prodImg = cartItem.image
                    .replace(/(\.[^.]*)$/, '_small$1')
                    .replace('http:', '');
            } else {
                prodImg = '//cdn.shopify.com/s/assets/admin/no-image-medium-cc9732cb976dd349a0df1d39816fbcc7.gif';
            }

            if (cartItem.properties !== null) {
                $.each(cartItem.properties, function (key, value) {
                    if (key.charAt(0) === '_' || !value) {
                        delete cartItem.properties[key];
                    }
                });
            }

            var noQty = false, noRemoved = false;
            if (cartItem.variant_title && cartItem.variant_title.indexOf("OFF") !== -1) {
                noQty = true;
                noRemoved = true;
                if (cartItem.price > 0) {
                    noRemoved = false;
                }
            }

            item = {
                key: cartItem.key,
                line: index + 1,
                url: subStringUrl(cartItem.url),
                img: prodImg,
                name: cartItem.product_title,
                variation: subStringVariantTitle(cartItem.variant_title),
                properties: cartItem.properties,
                itemAdd: cartItem.quantity + 1,
                itemMinus: cartItem.quantity - 1,
                itemQty: cartItem.quantity,
                price: theme.Currency.formatMoney(cartItem.price, settings.moneyFormat),
                line_price: theme.Currency.formatMoney(cartItem.line_price, settings.moneyFormat),
                discountedPrice: theme.Currency.formatMoney(
                    cartItem.price - cartItem.total_discount / cartItem.quantity,
                    settings.moneyFormat
                ),
                discounts: cartItem.discounts,
                discountsApplied:
                    cartItem.price === cartItem.price - cartItem.total_discount
                        ? false
                        : true,
                vendor: cartItem.vendor,
                variant_id: cartItem.variant_id,
                noQty: noQty,
                noRemoved: noRemoved,
            };

            items.push(item);

        });

        // Gather all cart data and add to DOM
        data = {
            items: items,
            note: cart.note,
            totalPrice: theme.Currency.formatMoney(
                cart.total_price,
                settings.moneyFormat
            ),
            totalCartDiscount:
                cart.total_discount === 0
                    ? 0
                    : theme.strings.cartSavings.replace(
                        '[savings]',
                        theme.Currency.formatMoney(
                            cart.total_discount,
                            settings.moneyFormat
                        )
                    )
        };

        $cartContainer.append(template(data));

        cartCallback(cart);
    };

    cartCallback = function (cart) {
        bd_AjaxCartResetCheckoutButton();
        $body.removeClass('drawer--is-loading');
        $body.trigger('ajaxCart.afterCartLoad', cart);

        if (window.Shopify && Shopify.StorefrontExpressButtons) {
            Shopify.StorefrontExpressButtons.initialize();
        }
    };

    adjustCart = function () {
        // Delegate all events because elements reload with the cart

        // Add or remove from the quantity
        $body.on('click', '.ajaxcart__qty-adjust', function () {
            if (isUpdating) {
                return;
            }
            var $el = $(this),
                line = $el.data('line'),
                variant_id = $el.data('variant-id'),
                $qtySelector = $el.siblings('.ajaxcart__qty-num'),
                qty = parseInt($qtySelector.val().replace(/\D/g, ''));

            qty = validateQty(qty);

            // Add or subtract from the current quantity

            if ($el.hasClass('ajaxcart__qty--remove')) {
                qty = 0;
            } else {
                if ($el.hasClass('ajaxcart__qty--plus')) {
                    qty += 1;
                } else {
                    qty -= 1;
                    if (qty <= 0) qty = 0;
                }
            }



            // If it has a data-line, update the cart.
            // Otherwise, just update the input's number
            if (variant_id) {
                updateQuantity(variant_id, line, qty);
            } else {
                $qtySelector.val(qty);
            }
        });

        // Update quantity based on input on change
        $body.on('change', '.ajaxcart__qty-num', function () {
            if (isUpdating) {
                return;
            }
            var $el = $(this),
                line = $el.data('line'),
                variant_id = $el.data('variant-id'),
                qty = parseInt($el.val().replace(/\D/g, ''));

            qty = validateQty(qty);

            // If it has a data-line, update the cart
            if (variant_id) {
                updateQuantity(variant_id, line, qty);
            }
        });

        // Prevent cart from being submitted while quantities are changing
        $body.on('submit', 'form.ajaxcart', function (evt) {
            if (isUpdating) {
                evt.preventDefault();
            }
        });

        // Highlight the text when focused
        $body.on('focus', '.ajaxcart__qty-adjust', function () {
            var $el = $(this);
            setTimeout(function () {
                $el.select();
            }, 50);
        });

        function updateQuantity(variant_id, line, qty) {
            isUpdating = true;

            // Add activity classes when changing cart quantities
            var $row = $('.ajaxcart__row[data-line="' + line + '"]').addClass(
                'is-loading'
            );

            if (qty === 0) {
                $row.parent().addClass('is-removed');
            }

            // Slight delay to make sure removed animation is done
            $(".ajaxcart__footer button.cart__checkout").remove();
            $(".ajaxcart__footer span.cart__checkout").show();
            setTimeout(function () {
                $.post('/cart/update.js', "updates[" + variant_id + "]=" + qty + "").always(function (res) {
                    adjustCartCallback(JSON.parse(res.responseText));
                });
                // ShopifyAPI.changeItem(line, qty, adjustCartCallback);
            }, 250);
        }

        // Save note anytime it's changed
        $body.on('change', 'textarea[name="note"]', function () {
            var newNote = $(this).val();

            // Update the cart note in case they don't click update/checkout
            ShopifyAPI.updateCartNote(newNote, function () { });
        });
    };

    adjustCartCallback = function (cart) {
        // Update quantity and price
        updateCountPrice(cart);

        // Reprint cart on short timeout so you don't see the content being removed
        setTimeout(function () {
            bundleBuildCart(cart);
            ShopifyAPI.getCart(bundleBuildCart);
            isUpdating = false;
        }, 500);
    };

    qtySelectors = function () {
        // Change number inputs to JS ones, similar to ajax cart but without API integration.
        // Make sure to add the existing name and id to the new input element
        var $numInputs = $('input[type="number"]');

        if ($numInputs.length) {
            $numInputs.each(function () {
                var $el = $(this),
                    currentQty = $el.val(),
                    inputName = $el.attr('name'),
                    inputId = $el.attr('id');

                var itemAdd = currentQty + 1,
                    itemMinus = currentQty - 1,
                    itemQty = currentQty;

                var source = $('#JsQty').html(),
                    template = Handlebars.compile(source),
                    data = {
                        key: $el.data('id'),
                        itemQty: itemQty,
                        itemAdd: itemAdd,
                        itemMinus: itemMinus,
                        inputName: inputName,
                        inputId: inputId
                    };

                // Append new quantity selector then remove original
                $el.after(template(data)).remove();
            });

            // Setup listeners to add/subtract from the input
            $('.js-qty__adjust').on('click', function () {
                var $el = $(this),
                    $qtySelector = $el.siblings('.js-qty__num'),
                    qty = parseInt($qtySelector.val().replace(/\D/g, ''));

                qty = validateQty(qty);

                // Add or subtract from the current quantity
                if ($el.hasClass('js-qty__adjust--plus')) {
                    qty += 1;
                } else {
                    qty -= 1;
                    if (qty <= 1) qty = 1;
                }

                // Update the input's number
                $qtySelector.val(qty);
            });
        }
    };

    validateQty = function (qty) {
        if (parseFloat(qty) === parseInt(qty) && !isNaN(qty)) {
            // We have a valid number!
        } else {
            // Not a number. Default to 1.
            qty = 1;
        }
        return qty;
    };

    module = {
        init: init,
        load: loadCart
    };

    return module;

})(ajaxCart || {}, jQuery);