function productBundleInit() {
    bundleProductId = null, bundleProductIsOffer = false, bundleDiscountInfo = null, hasCrossSellsItemAdded = false, bundleJsonData = [], bundleOfferSelected = {};
    bundleShopDomain = "{{shopDomain}}";
    bundleServerUrl = "{{apiURI}}";
    bundleJsonUrl = "{{bundleData}}?v=" + (new Date().getTime());
    bundleStyleUrl = "{{styleURI}}?v=" + (new Date().getTime());

    if (window.ShopifyAnalytics) {
        if (window.ShopifyAnalytics.meta && window.ShopifyAnalytics.meta.page) {
            if (window.ShopifyAnalytics.meta.page.pageType === 'product') {
                bundleProductId = window.ShopifyAnalytics.meta.page.resourceId;
            }
        }
    }

    var bundleLinkStylesheet = this.document.createElement('link');
    bundleLinkStylesheet.setAttribute('rel', 'stylesheet');
    bundleLinkStylesheet.setAttribute('type', 'text/css');
    bundleLinkStylesheet.setAttribute('href', bundleStyleUrl);
    document.head.appendChild(bundleLinkStylesheet);
}

(function () {
    productBundleInit();
    var template = bundleGetTemplate();

    if (template === "product") {
        if (!bundleProductId) {
            return;
        }

        bundleMakeRequest("GET", bundleJsonUrl, function (response) {
            var bundleData = JSON.parse(response);
            if (!bundleData.length) return;
            bundleJsonData = bundleData;
            bundleGenerateGroups();
            bundleGererateOffers();
        });

        if ($(".product-form__input").length) {
            $(".product-form__input option").each(function () {
                if ($(this).val().indexOf("+") > -1 || $(this).val().indexOf("%") > -1) {
                    $(this).remove();
                }
            });

            $(".product-form__input").change(function () {
                var getCustomVariant = bundleGetCustomVariantOnSelect();
                bundleOfferSelected.total_id = getCustomVariant.variantTotalId;
                bundleOfferSelected.free_id = getCustomVariant.variantFreeId;
            });
        }

        $("button.btn--add-to-cart[type=submit]").click(function () {
            if ($("#CartDrawer").length) {
                bundleAjaxCart.init();
                setTimeout(function () {
                    bundleAjaxCart.load();
                }, 1500);
            }
        });
    }

    if (template === "cart") {
        bundleRenderTotals();
        var checkout_selectors = ["input[name='checkout']", "button[name='checkout']", "[href$='checkout']"];
        checkout_selectors.forEach(function (selector) {
            var els = document.querySelectorAll(selector);
            if (typeof els == "object" && els) {
                for (var i = 0; i < els.length; i++) {
                    var el = els[i];
                    if (typeof el.addEventListener != "function") {
                        return;
                    }
                    el.addEventListener("click", function (e) {
                        e.preventDefault();
                        el.innerHTML = 'Processing... please wait.';
                        bundleOnClickCheckout();
                    }, false);
                }
            }
        });
    }
})(window);

function bundleOnClickCheckout() {
    var checkOutUrlParams = {};
    var checkOutUrl = "/checkout";
    if (!bundleDiscountInfo) {
        window.location.href = checkOutUrl;
    } else {
        bundleMakeRequest('POST', bundleServerUrl + '/assets/generate_discount_code/' + bundleShopDomain, bundleDiscountInfo, function (response) {
            var data = response;
            if (!data) {
                return;
            }
            try {
                data = JSON.parse(response);
                if (data.code) {
                    checkOutUrlParams.discount = data.code;
                }
                if (Object.keys(checkOutUrlParams).length) {
                    checkOutUrl += "?" + bundleQueryString(checkOutUrlParams);
                }
            }
            catch (e) {
            }
            window.location.href = checkOutUrl;
        });
    }
}

function bundleRenderTotals() {
    bundleGetBundleTypes(function (data, cart, items) {
        var groups = data.groups;
        var offers = data.offers;
        var cross_sells = data.cross_sells;
        bundleCalculatePriceItems(cart, items, groups, cross_sells, offers);
        bundleCalculatePriceTotal(cart, groups, cross_sells);
    });
}

function bundleCalculatePriceItems(cart, items, groups, cross_sells, offers) {
    if (offers.length && !groups.length && !cross_sells.length) {
        bundleOfferRenderCartItem(cart, offers);
    } else if (offers.length && groups.length && !cross_sells.lengt) {
        bundleOfferRenderCartItem(cart, offers);
        bundleGroupRenderCartItem(groups);
    } else if (groups.length && cross_sells.length && !offers.length) {
        bundleGroupCrossSellRenderCartItem(items, groups, cross_sells);
    } else if (groups.length && !cross_sells.length && !offers.length) {
        bundleGroupRenderCartItem(groups);
    } else if (cross_sells.length && !groups.length && !offers.length) {
        bundleCrossSellRenderCartItem(items, cross_sells);
    }
}

function bundleGroupRenderCartItem(groups) {
    groups.forEach(function (bundle) {
        bundle.products.forEach(function (e) {
            bundleRenderItemsPricesHtml(e);
        });
    });
}

function bundleCrossSellRenderCartItem(items, cross_sells) {
    cross_sells.forEach(function (bundle) {
        var offerProducts = bundleOnlyGetProductsInCart(bundle.cross_sell.offerProducts, items);
        offerProducts.forEach(function (e) {
            bundleRenderItemsPricesHtml(e);
        });
    });
}

function bundleOfferRenderCartItem(cart, offers) {
    bundleOfferGetCart(cart, offers, function (cartItems) {
        if (cartItems.length) {
            var noteCart = {};
            cartItems.forEach(function (e) {
                noteCart[e.variant_id] = e.quantity;
            });
            $.post('/cart/update.js', { updates: noteCart }).always(function () {
                window.location.reload();
            });
        }
    });
}

function bundleOfferGetCart(cart, offers, callback) {
    var tmp = {}, originItems = cart.items, filteredItems = [], freeItems = [], subItems = [], mainItems = [], rollCart = [];

    // chi lay cac Variant Free
    freeItems = originItems.filter(function (i) { return i.variant_title && i.variant_title.indexOf("Free") !== -1 });
    freeItems.forEach(function (e) {
        var hasMainVariant = originItems.find(function (o) {
            return o.product_id == e.product_id && (!o.variant_title || o.variant_title && o.variant_title.indexOf("Free") <= -1);
        });
        if (!hasMainVariant) {
            mainItems.push({
                title: e.title,
                product_id: e.product_id,
                variant_id: e.variant_id,
                quantity: 0,
            });
        }
    });

    // loai bo cac Variant Free
    filteredItems = originItems.filter(function (i) { return !i.variant_title || i.variant_title.indexOf("Free") <= -1 });

    // gop tong so luong cho moi san pham
    filteredItems.forEach(function (e) {
        if (!tmp[e.product_id] || tmp[e.product_id] === undefined) tmp[e.product_id] = [];
        tmp[e.product_id].push(e);
    });
    for (let id in tmp) {
        var quantity = 0;
        var title = tmp[id][0].title;
        var variant_id = tmp[id][0].variant_id;
        var variant_title = tmp[id][0].variant_title;
        var original_line_price = tmp[id][0].original_line_price;
        var original_price = tmp[id][0].original_price;
        var line_price = tmp[id][0].line_price;
        var price = tmp[id][0].price;
        tmp[id].forEach(function (e) { quantity += e.quantity });
        subItems.push({ product_id: parseFloat(id), quantity: quantity, title: title, line_price: line_price, original_line_price: original_line_price, original_price: original_price, price: price, variant_title: variant_title, variant_id: variant_id });
    }

    subItems.forEach(function (item) {
        // kiem tra co bundle nao bao gom san pham nay khong
        var bundle = offers.find(function (o) { return o.offer.productIds.indexOf(item.product_id) != -1 });
        if (!bundle) return;

        // get thong tin sp tu bundle
        var thisProduct = bundle.offer.products.find(function (p) { return p.id == item.product_id });

        // get Price Levels tu bundle
        var priceLevels = bundle.offer.price_levels.sort(function (a, b) { return b.origin_qty - a.origin_qty });

        // đo dai của Price Levels
        var priceLevelLength = priceLevels.length;

        // khai bao bien reset trang thai upselling ve variant goc
        var isReset = true;

        // var variantTotalMeta = thisProduct.variantTotals.find(function (e) { return item.variant_id == e.origin_variant_id || item.variant_id == e.custom_variant_id });
        // var variantFreeMeta = thisProduct.variantFrees.find(function (e) { return item.variant_id == e.origin_variant_id || item.variant_id == e.custom_variant_id });
        var variantTotalMeta = thisProduct.variantTotal;
        var variantFreeMeta = thisProduct.variantFree;

        for (let i = 0; i < priceLevelLength; i++) {
            if (item.quantity >= priceLevels[i].origin_qty) {
                isReset = false;

                // dung refresh gio hang neu Variant Free da ton tai và Quantity cua Variant Free khong thay doi
                var variantFree = originItems.find(function (e) { return e.variant_id == variantFreeMeta.custom_variant_id });
                var variantUpsell = originItems.find(function (e) { return e.variant_id == variantTotalMeta.custom_variant_id });

                if (variantFree && priceLevels[i].free_qty == variantFree.quantity && variantUpsell.quantity == item.quantity) {
                    break;
                }

                // them Variant Free và Variant Total vào gio hang
                mainItems.push(
                    {
                        title: item.title,
                        quantity: item.quantity,
                        product_id: item.product_id,
                        variant_id: variantTotalMeta.custom_variant_id,
                    },
                    {
                        title: item.title,
                        quantity: priceLevels[i].free_qty,
                        product_id: item.product_id,
                        variant_id: variantFreeMeta.custom_variant_id,
                    }
                );
                break;
            }
        }

        // chuyen ve Variant goc
        if (isReset && item.variant_id == variantTotalMeta.custom_variant_id) {
            mainItems.push(
                {
                    title: item.title,
                    quantity: item.quantity,
                    product_id: item.product_id,
                    variant_id: variantTotalMeta.origin_variant_id,
                },
            );
        }

    });

    // duyet qua cac items trong gio hang
    if (mainItems.length) {
        var reset = [];
        originItems.forEach(function (origin) {
            var t = {
                title: origin.title,
                quantity: origin.quantity,
                product_id: origin.product_id,
                variant_id: origin.variant_id,
            };
            var hasMain = mainItems.find(function (main) { return main.product_id == origin.product_id });
            if ((hasMain && origin.variant_title && origin.variant_title.indexOf("%")) || (hasMain && !origin.variant_title)) {
                t.quantity = 0;
            }
            reset.push(t);
        });
        rollCart = reset.concat(mainItems);
    }

    return callback(rollCart);
}

function bundleAppendOfferItemsHtml(product) {
    if ($(".cart__row__" + product.variant_id).length) {
        var percent = product.percent == 100 ? 'Free' : product.percent + 'Sale % OFF';
        var dom = $(".cart__row__" + product.variant_id).clone();
        dom.addClass('cart-item-offer');
        dom.find('.cart__product-meta').remove();
        dom.find('.grid__item.two-thirds').append('<p class="cart__product-meta offer-item-meta">' + percent + '</p>');
        dom.find('.js-qty__adjust').remove();
        dom.find('.js-qty__num').val(product.quantity).attr('disabled', true);
        dom.find('.cart__price__item').html('<span class="bundle-cart-item-line-price bold-bundles-cart-item__price--old money">$' + product.old_price + ' USD</span>&nbsp;<span class="bundle-cart-item-discount-price money">$' + product.new_price + ' USD</span>');
        dom.insertAfter(".cart__row__" + product.variant_id);
    }
}

function bundleRenderItemsPricesHtml(product) {
    var oldPrice = product.line_price;
    var newPrice = (Number(product.new_price) * product.line_quantity).toFixed(2);
    if ($(".cart__row__" + product.id + ".cart__row__" + product.variant_id).length) {
        // $(".cart__row__" + product.id + ".cart__row__" + product.variant_id).find(".cart__price__item").html('<span class="bundle-cart-item-line-price bold-bundles-cart-item__price--old money">$' + oldPrice + ' USD</span>&nbsp;<span class="bundle-cart-item-discount-price money">$' + newPrice + ' USD</span>');
        $(".cart__row__" + product.id + ".cart__row__" + product.variant_id).find(".cart__price__item .lion-cart-item-line-price .money").html('$' + newPrice + ' USD');
    }
}

function bundleGetBundleTypes(callback) {
    var cartItems = [];
    $.get('/cart.js').always(function (res) {
        if (res && res.status == 200) {
            var cart = JSON.parse(res.responseText);

            if (!cart.items.length) {
                return;
            }

            cart.items.forEach(function (e) {
                cartItems.push({
                    id: e.product_id,
                    variant_id: e.id,
                    variant_title: e.variant_title,
                    line_quantity: e.quantity,
                    line_price: e.line_price / 100,
                    original_price: e.original_price / 100,
                });
            });

            bundleMakeRequest("GET", bundleJsonUrl, function (response) {
                var bundleData = response;
                if (!bundleData) {
                    return;
                }
                try {
                    bundleData = JSON.parse(response);
                    bundleJsonData = bundleData;
                    var bundleGroups = bundleGetBundleTypeGroup(bundleData, cartItems);
                    var bundleOffers = bundleGetBundleTypeOffers(bundleData, cartItems);
                    var bundleCrossSells = bundleGetBundleTypeCrossSells(bundleData, cartItems);
                    return callback({
                        groups: bundleGroups,
                        offers: bundleOffers,
                        cross_sells: bundleCrossSells,
                    }, cart, cartItems);
                }
                catch (err) {
                    return console.log(err);
                }
            });
        }
    });
}

function bundleGetBundleTypeGroup(bundleData, items) {
    var bundleArr = [], bundleResults = [];
    items.forEach(function (e) {
        var r = bundleData.find(function (obj) {
            return obj.type === 'group' && obj.productIds.indexOf(e.id) != -1;
        });
        if (r) bundleArr.push(r);
    });
    if (!bundleArr.length) {
        return [];
    }
    bundleArr = bundleUniqueArray(bundleArr);
    bundleArr.forEach(function (bundle) {
        var prodArr = [], bundleQuantity = 0, bundlePrice = 0;
        prodArr = bundleOnlyGetProductsInCart(bundle.products, items);
        bundle.products = prodArr;
        prodArr.forEach(function (e) {
            bundleQuantity += e.line_quantity;
            bundlePrice += e.line_price;
        });
        bundlePrice = bundlePrice.toFixed(2);
        if (bundleQuantity >= Object.keys(bundle.products).length && bundlePrice >= bundle.old_total) {
            bundleResults.push(bundle);
        }
    });
    return bundleResults;
}

function bundleGetBundleTypeCrossSells(bundleData, items) {
    var bundleArr = [], getCrossSellss = [], fullCrossSells = [], partialCrossSells = [];
    items.forEach(function (e) {
        var r = bundleData.find(function (obj) {
            return obj.type === 'cross_sell' && obj.cross_sell.triggerProductIds.indexOf(e.id) != -1;
        });
        if (r) bundleArr.push(r);
    });
    if (!bundleArr.length) {
        return [];
    }
    getCrossSellss = bundleGetCrossSellsExists(bundleArr, items);
    fullCrossSells = getCrossSellss.fullCrossSellsProducts;
    partialCrossSells = getCrossSellss.partialCrossSellsProducts;
    if (partialCrossSells.length) {
        bundleShowCrossSellsPopup(partialCrossSells);
    }
    if (fullCrossSells.length) {
        fullCrossSells.forEach(function (bundle) {
            bundle.cross_sell.offerProducts = bundleAssignLineQty(bundle.cross_sell.offerProducts, items);
            return bundle;
        });
    }
    return fullCrossSells;
}

function bundleGetBundleTypeOffers(bundleData, items) {
    var bundleArr = [];
    items.forEach(function (e) {
        var r = bundleData.find(function (obj) {
            return obj.type === 'offer' && obj.offer.productIds.indexOf(e.id) != -1;
        });
        if (r) bundleArr.push(r);
    });
    if (!bundleArr.length) {
        return [];
    }
    bundleArr = bundleUniqueArray(bundleArr);
    return bundleArr;
}

function bundleCalculatePriceTotal(cart, groups, cross_sells) {
    var subTotal = 0, originalTotal = cart.original_total_price / 100;
    if (groups.length && cross_sells.length) {
        subTotal = bundleAllSubTotal(groups, cross_sells, cart);
        bundleRenderSubTotalHtml(originalTotal, subTotal);
    } else if (groups.length && !cross_sells.length) {
        subTotal = bundleGroupSubTotal(groups, cart);
        bundleRenderSubTotalHtml(originalTotal, subTotal);
    } else if (!groups.length && cross_sells.length) {
        subTotal = bundleCrossSellSubTotal(cross_sells, cart);
        bundleRenderSubTotalHtml(originalTotal, subTotal);
    }
}

function bundleAllSubTotal(groups, cross_sells, cart) {
    var subTotal = 0;
    cart.items.forEach(function (e) {
        var sub = e.price / 100;
        var bundleGroup = groups.find(function (b) { return b.productIds.indexOf(e.product_id) > -1 });
        var bundleCrossSell = cross_sells.find(function (b) { return b.cross_sell.offerProductIds.indexOf(e.product_id) > -1 });
        if (bundleGroup && bundleCrossSell) {
            var percent = bundleGroup.percent;
            if (bundleGroup.discount_method === "fixed_amount") {
                var new_price = (sub - bundleGroup.fixed_price).toFixed(2);
                new_price = new_price > 0 ? new_price : 0;
                percent = (100 - ((new_price / sub) * 100)).toFixed(2);
            }
            percent = (parseFloat(percent) + parseFloat(bundleCrossSell.cross_sell.discountValue)) / 100;
            sub = (sub - (sub * percent)) * e.quantity;
            subTotal += parseFloat(sub.toFixed(2));
        } else if (bundleGroup && !bundleCrossSell) {
            if (bundleGroup.discount_method === "percentage") {
                sub = (sub - (sub * bundleGroup.percent) / 100) * e.quantity;
                subTotal += parseFloat(sub.toFixed(2));
            } else if (bundleGroup.discount_method === "fixed_amount") {
                sub = sub * e.quantity;
                subTotal += parseFloat(sub.toFixed(2));
                subTotal = subTotal - bundleGroup.fixed_price;
                subTotal = subTotal > 0 ? subTotal : 0;
            }
        } else if (!bundleGroup && bundleCrossSell) {
            sub = (sub - (sub * bundleCrossSell.cross_sell.discountValue) / 100) * e.quantity;
            subTotal += parseFloat(sub.toFixed(2));
        } else {
            sub = sub * e.quantity;
            subTotal += parseFloat(sub.toFixed(2));
        }
    });
    return subTotal.toFixed(2);
}

function bundleGroupSubTotal(groups, cart) {
    var subTotal = 0;
    cart.items.forEach(function (e) {
        var sub = e.price / 100;
        var bundle = groups.find(function (b) { return b.productIds.indexOf(e.product_id) > -1 });
        if (bundle) {
            if (bundle.discount_method === "percentage") {
                sub = (sub - (sub * bundle.percent) / 100) * e.quantity;
                subTotal += parseFloat(sub.toFixed(2));
            } else if (bundle.discount_method === "fixed_amount") {
                sub = sub * e.quantity;
                subTotal += parseFloat(sub.toFixed(2));
                subTotal = subTotal - bundle.fixed_price;
                subTotal = subTotal > 0 ? subTotal : 0;
            }
        } else {
            sub = sub * e.quantity;
            subTotal += parseFloat(sub.toFixed(2));
        }
    });
    return subTotal.toFixed(2);
}

function bundleCrossSellSubTotal(cross_sells, cart) {
    var subTotal = 0;
    cart.items.forEach(function (e) {
        var sub = e.price / 100;
        var bundle = cross_sells.find(function (b) { return b.cross_sell.offerProductIds.indexOf(e.product_id) > -1 });
        if (bundle) {
            sub = (sub - (sub * bundle.cross_sell.discountValue) / 100) * e.quantity;
        } else {
            sub = sub * e.quantity;
        }
        subTotal += parseFloat(sub.toFixed(2));
    });
    return subTotal.toFixed(2);
}

function bundleRenderSubTotalHtml(originalTotal, subTotal) {
    var savingTotal = (originalTotal - subTotal).toFixed(2);
    bundleDiscountInfo = { originalTotal, subTotal, savingTotal };
    if ($(".cart__subtotal").length) {
        // var dom = '';
        // dom += "<span class='bundle-original-cart-total money'>$" + originalTotal + " USD</span>";
        // dom += "<span class='bundle-line-cart-total' style='display:block'>Enjoy your savings <span class='bundle-cart-total-item money'>$" + savingTotal + " USD</span></span>";
        // dom += "<span class='bundle-line-discount money' style='display:block;font-weight:700'>$" + subTotal + " USD</span>";
        // $(".cart__subtotal").html(dom);
        $(".cart__subtotal .bundle-line-discount").html("$" + subTotal + " USD");
    }
}

function bundleGroupCrossSellRenderCartItem(items, groups, cross_sells) {
    var allProducts = [], newProducts = [];
    var productsInGroups = bundleGetProductsFromBundleGroups(groups);
    var productsInCrossSellss = bundleGetProductsFromBundleCrossSells(cross_sells, items);
    allProducts = productsInGroups.concat(productsInCrossSellss);
    allProducts.forEach(function (p) {
        var r = newProducts.find(function (e) { return e.id === p.id });
        if (r) {
            p.percent += r.percent;
            p.new_price = Number((p.old_price - (p.old_price * (p.percent / 100))).toFixed(2));
            newProducts = newProducts.filter(function (e) { return e.id !== p.id });
        }
        newProducts.push(p);
    });
    newProducts.forEach(function (e) {
        bundleRenderItemsPricesHtml(e);
    });
}

function bundleGetProductsFromBundleGroups(bundles) {
    var productArr = [];
    bundles.forEach(function (bundle) {
        bundle.products.forEach(function (p) {
            var percent = bundle.percent;
            if (bundle.discount_method == 'fixed_amount') {
                percent = (100 - ((p.new_price / p.old_price) * 100)).toFixed(2);
            }
            productArr.push({
                id: p.id,
                title: p.title,
                line_price: p.line_price,
                line_quantity: p.line_quantity,
                old_price: p.old_price,
                new_price: p.new_price,
                percent: Number(percent),
            });
        });
    });
    return productArr;
}

function bundleGetProductsFromBundleCrossSells(cross_sells, items) {
    var productArr = [];
    cross_sells.forEach(function (bundle) {
        var offerProducts = bundleOnlyGetProductsInCart(bundle.cross_sell.offerProducts, items);
        offerProducts.forEach(function (p) {
            productArr.push({
                id: p.id,
                title: p.title,
                line_price: p.line_price,
                line_quantity: p.line_quantity,
                old_price: p.old_price,
                new_price: p.new_price,
                percent: bundle.cross_sell.discountValue,
            });
        });
    });
    return productArr;
}

function bundleGetCrossSellsExists(bundleArr, items) {
    var fullCrossSellsProducts = [], partialCrossSellsProducts = [];
    bundleArr = bundleUniqueArray(bundleArr);
    items.forEach(function (e) {
        var r = bundleArr.find(function (obj) {
            return obj.cross_sell.offerProductIds.indexOf(e.id) != -1;
        });
        if (r) {
            fullCrossSellsProducts.push(r);
            bundleArr = bundleArr.filter(function (obj) { return obj._id != r._id });
        }
    });
    return {
        fullCrossSellsProducts,
        partialCrossSellsProducts: bundleArr,
    };
}

function bundleShowCrossSellsPopup(partialGroups) {
    if (!bundleGetCookie("__bundleCrossSellClosed")) {
        setTimeout(function () {
            bundleCrossSellPopupHtml(partialGroups);
        }, 2000);
    }
}

function bundleCrossSellPopupHtml(groups) {
    var html = '';
    html += '<div id="bundlePopupCrossSellss" class="jquery-dpModal current blocker">';
    html += '<div id="dpModal-container" class="dp-popup dp-wrap dp-whModal dp-popup-dpModal">';
    groups.forEach(function (bundle) {
        var countProducts = bundle.cross_sell.offerProducts.length;
        html += '<div class="multiple-products-true ba-product-bundle clearfix">';
        html += '<h3 class="upsell-title">Buy ' + countProducts + ' for ' + bundle.cross_sell.discountValue + '% off</h3>';
        bundle.cross_sell.offerProducts.forEach(function (p) {
            var variants = bundleFilterVariants(p.variants);
            var multi_variants_class = (variants.length > 1) ? 'multiple-variants' : '';
            html += '<div class="product-container discount-applies-true" data-product-id="' + p.id + '" data-variant-id="' + variants[0].id + '">';
            html += '<div class="image">';
            html += '<a href="/products/' + p.handle + '" target="_blank">';
            html += '<img src="' + p.image_url + '">';
            html += '</a>';
            html += '</div>';
            html += '<div class="details ' + multi_variants_class + '">';
            html += '<div class="product-title">';
            html += '<a href="/products/' + p.handle + '" target="_blank">';
            html += p.title;
            html += '</a>';
            html += '</div>';
            html += '<div class="product-price">';
            html += '<span class="ba-sale price">$' + p.new_price.toFixed(2) + ' <s class="price">$' + p.old_price + '</s></span>';
            html += '</div>';
            html += '<div class="product-quantity">';
            html += '<input type="number" class="" value="1" min="1" max="999">';
            html += '</div>';
            html += '</div>';
            html += '<div class="actions">';
            if (variants.length > 1) {
                html += '<select>';
                variants.forEach(function (v) {
                    html += '<option value="' + v.id + '" data-price="' + v.price + '" data-compare-at-price="' + v.compare_at_price + '">' + v.title + '</option>';
                });
                html += '</select>';
            }
            html += '<button class="add-cross_sells" onclick="bundleAddCrossSellss(this)">Add to cart</button>';
            html += '</div>';
            html += '</div>';
        });
        html += '</div>';
    });
    html += '<div class="no-thanks">';
    html += '<button class="btn btn-success btnCompleteCrossSells" style="display:none" onclick="bundleCompleteCrossSells()">OK</button>';
    html += '<a href="javascript:void(0)" onclick="bundleCloseCrossSellsPopup(this, true)">No thanks</a>';
    html += '</div>';
    html += '<a href="javascript:void(0)" class="close-dpModal" onclick="bundleCloseCrossSellsPopup(this)">Close</a></div>';
    html += '</div>';
    $('body').append(html);
}

function bundleFilterVariants(variants) {
    variants = variants.filter(function (e) { return e.title.indexOf("+") <= -1 });
    return variants;
}

function bundleAddCrossSellss(obj) {
    var $this = $(obj);
    var $container = $this.closest('.product-container');
    var variant_id = $container.data('variant-id');
    var quantity = $container.find('.product-quantity input[type="number"]').val();
    var variants = $this.prev('select');
    if (variants.length) variant_id = variants.val();
    $.post('/cart/add.js', { id: variant_id, quantity: quantity }).always(function (res) {
        if (res && res.status == 200) {
            $this.addClass('disabled').attr('disabled', true).text('Item added');
            $this.closest('#dpModal-container').find('.btnCompleteCrossSells').show();
            hasCrossSellsItemAdded = true;
            return false;
        }
    });
}

function bundleCloseCrossSellsPopup(obj, hasCookie = false) {
    var $this = $(obj);
    $this.closest('#bundlePopupCrossSellss').removeClass('blocker');
    if (hasCookie) {
        var exdays = new Date();
        exdays.setTime(exdays.getTime() + (1 * 60 * 60 * 1000));
        bundleSetCookie("__bundleCrossSellClosed", 1, exdays);
    }
    if (hasCrossSellsItemAdded) {
        bundleCompleteCrossSells();
    }
}

function bundleCompleteCrossSells() {
    window.location.reload();
}

function bundleAssignLineQty(p1, p2) {
    var products = p1.map(function (p) { return Object.assign(p, p2.find(function (e) { return e.id == p.id })) });
    return products;
}

function bundleOnlyGetProductsInCart(bundleProducts, items) {
    var results = [];
    items.forEach(function (i) {
        if (bundleProducts.find(function (e) { return i.id == e.id })) {
            if (!i.variant_title || (i.variant_title && i.variant_title.indexOf("Free") <= -1)) {
                results.push(i);
            }
        }
    });
    results = bundleAssignLineQty(results, bundleProducts);
    return results;
}

function bundleUniqueArray(array) {
    var result = Array.from(new Set(array));
    return result;
}

function bundleGererateOffers() {
    if (!bundleJsonData.length) {
        return;
    }
    var bundle = bundleJsonData.find(function (obj) {
        return obj.offer.productIds.indexOf(bundleProductId) != -1;
    });
    if (!bundle || typeof bundle !== 'object') {
        return;
    }
    bundleProductIsOffer = true;
    var html = "";
    html += '<div class="flw buy-x-get-y-free">';
    html += '<a href="javascript:void(0)" onclick="bundleSelectOffer(this, 1, 0)">Get 1</a>';
    var priceLevels = bundle.offer.price_levels.sort(function (a, b) { return a.origin_qty - b.origin_qty });
    priceLevels.forEach(function (e) {
        html += '<a href="javascript:void(0)" onclick="bundleSelectOffer(this, ' + e.origin_qty + ', ' + e.free_qty + ')">Get ' + e.origin_qty + ' Free ' + e.free_qty + '</a>';
    });
    html += '</div>';

    if ($(".bundleOfferDisplayArea").length) {
        $(".bundleOfferDisplayArea").html(html);
    } else {
        $(".product-single form").prepend(html);
    }
}

function bundleSelectOffer(obj, origin_qty, free_qty) {
    $(".product-single__quantity").hide();
    $(".product-form__item").addClass("width-full");
    $(".btn-cart-bundle-offer").remove();

    if ($(obj).hasClass('active')) {
        bundleOfferSelected = {};
        $(obj).removeClass('active');
        $(".btn--add-to-cart").show();
        $(".product-single__quantity").show();
    } else {
        $(obj).closest('.buy-x-get-y-free').find('a').removeClass('active');
        $(obj).addClass('active');

        var getCustomVariant = bundleGetCustomVariantOnSelect();
        if (!getCustomVariant.variantFreeId && !getCustomVariant.variantTotalId) {
            return;
        }

        bundleOfferSelected.total_id = getCustomVariant.variantTotalId;
        bundleOfferSelected.free_id = getCustomVariant.variantFreeId;
        bundleOfferSelected.quantity = origin_qty;
        bundleOfferSelected.free_qty = free_qty;

        $(".btn--add-to-cart").hide();
        var btnAddCart = $(".btn--add-to-cart").clone();
        btnAddCart.attr('type', 'button');
        btnAddCart.attr('onclick', 'bundleAddToCartByOffer()');
        btnAddCart.addClass('btn-cart-bundle-offer');
        btnAddCart.show();
        btnAddCart.insertAfter($(".btn--add-to-cart"));
    }
}

function bundleGetCustomVariantOnSelect() {
    if (!bundleJsonData.length)
        return false;

    var variantId = $("#ProductSelect").val();

    var bundle = bundleJsonData.find(function (e) { return e.offer.productIds.indexOf(bundleProductId) !== -1 });
    if (!bundle)
        return false;

    var product = bundle.offer.products.find(function (p) { return p.id == bundleProductId });
    if (!product)
        return false;

    var variantTotalId = product.variantTotal.origin_variant_id;
    var variantFreeId = product.variantTotal.custom_variant_id;

    return { variantTotalId, variantFreeId };
}

function bundleAddToCartByOffer() {
    if (Object.keys(bundleOfferSelected).length) {
        $.post('/cart/update.js', "updates[" + bundleOfferSelected.free_id + "]=" + bundleOfferSelected.quantity + "&updates[" + bundleOfferSelected.total_id + "]=0").always(function () {
            if ($("#CartDrawer").length) {
                bundleAjaxCart.init();
                bundleAjaxCart.load();
                timber.RightDrawer.open();
            }
        });
    }
}

function bundleGenerateGroups() {
    if (!bundleJsonData.length) {
        return;
    }

    var result = bundleJsonData.find(function (obj) {
        return obj.productIds.indexOf(bundleProductId) != -1;
    });
    if (!result || typeof result !== 'object' || Object.keys(result.products).length === 0) {
        return;
    }
    var html = "<div id='product_bundles' class='bold-bundles-widget basic-bundle'>";
    html += '<div class="bold-bundles-widget-header"><h3 class="bold-bundles-widget-header__title"></h3></div>';
    html += "<div class='bold-bundles-widget__items'>";
    result.products.forEach(function (product) {
        html += "<div class='bold-bundles-widget-item bold-bundles-widget-item--product'>";
        html += "<div class='bold-bundles-widget-item__wrapper'>";
        html += "<div class='bold-bundles-widget-item__thumbnail'>";
        html += "<a class='bold-bundles-widget-item__link' href='/products/" + product.handle + "' target='_blank'><img class='bold-bundles-widget-item__image' src='" + product.image_url + "'></a>";
        html += "</div>";
        html += "<div class='bold-bundles-widget-item__info'>";
        html += "<h4 class='bold-bundles-widget-item__title'>" + product.title + " x " + product.product_quantity + "</h4>";

        if (product.variants) {
            var hidden = 'hidden';
            // if (product.variants[0].title === "Default Title") {
            //     hidden = 'hidden';
            // }
            html += "<select class='bold-bundles-widget-item__variants " + hidden + "' onchange='bundleChangeVariantOption(this)'>";
            product.variants.forEach(function (variant) {
                html += '<option class="bold-bundles-widget-item__variant" value="' + variant.id + '" data-original-price="' + variant.price + '" data-price="' + variant.bundle_price + '" data-quantity="' + product.product_quantity + '">' + variant.title + '</option>';
            });
            html += "</select>";

            html += "<div class='bold-bundles-widget-item__price'>";
            html += "<span class='bold-bundles-widget-item__price--old money'>$" + product.variants[0].price + " USD</span>&nbsp;";
            html += "<span class='bold-bundles-widget-item__price--new money'>$" + product.variants[0].bundle_price + " USD</span>";
            html += "</div>";
        }

        html += "</div>";
        html += "</div>";
        html += "</div>";
        html += '<div class="bold-bundles-widget-item bold-bundles-widget-item--with-separator"><div class="bold-bundles-widget-item-separator__wrapper"> <span class="bold-bundles-widget-item-separator"><svg class="bold-bundles-widget-item-separator__icon"><use xlink:href="#bold-plus-sign"><svg id="bold-plus-sign" viewBox="0 0 472 472" width="100%" height="100%"><path d="M472,185H287V0H185v185H0v102h185v185h102V287h185V185z"></path></svg></use></svg></span></div></div>';
    });
    html += "</div>";
    html += "<div class='bold-bundles-widget__footer'>";
    html += '<button class="btn bold-bundles-widget__button" type="button" onclick="bundleClickBundleToCart(this)"><span class="bold-bundles-widget__button--top">Add Bundle to Cart</span></button>';
    html += "</div>";
    html += "</div>";

    if ($(".bundleDisplayArea").length) {
        $(".bundleDisplayArea").html(html);
    } else {
        $(".product-single form").append(html);
    }
}

function bundleChangeVariantOption(obj) {
    var oldPrice = $(obj).find(':selected').data('original-price');
    var newPrice = $(obj).find(':selected').data('price');
    $(obj).next('.bold-bundles-widget-item__price').find('.bold-bundles-widget-item__price--old').html("$" + oldPrice + " USD");
    $(obj).next('.bold-bundles-widget-item__price').find('.bold-bundles-widget-item__price--new').html("$" + newPrice + " USD");
}

function bundleClickBundleToCart(obj) {
    var cartItems = [], _timeOut = 100;
    $(obj).parents("#product_bundles").find(".bold-bundles-widget-item__variants").each(function () {
        cartItems.push({
            id: parseInt($(this).val()),
            quantity: parseInt($(this).find(':selected').data('quantity')),
        });
    });
    cartItems.forEach(function (element) {
        setTimeout(function () {
            $.post('/cart/add.js', element);
        }, _timeOut);
        _timeOut += 500;
    });
    setTimeout(function () {
        window.location.href = "/cart";
    }, _timeOut);
}

function bundleMakeRequest(method, endpoint, data, callback) {
    var xmlhttp = new XMLHttpRequest();
    if (bundleDetectHttps(endpoint) && !bundleDetectServerURI(endpoint)) {
        xmlhttp.withCredentials = true;
    }
    if (method === "GET") {
        if (typeof data === 'function') {
            callback = data;
            data = null;
        } else {
            endpoint += '?' + bundleQueryString(data);
            data = null;
        }
    }
    xmlhttp.open(method, endpoint, true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status === 200) {
            callback(xmlhttp.responseText);
        }
    }
    xmlhttp.send(JSON.stringify(data));
}

function bundleQueryString(obj) {
    var str = [];
    for (var p in obj) {
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    }
    return str.join("&");
}

function bundleDetectHttps(url) {
    if (url.indexOf("https://") == 0) {
        return true;
    }
    return false;
}

function bundleDetectServerURI(url) {
    if (url.indexOf(bundleServerUrl) == 0) {
        return true;
    }
    return false;
}

function bundleSetCookie(cname, cvalue, exdays) {
    var expires = "expires=" + exdays;
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function bundleGetCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function bundleGetTemplate() {
    var classTemplate = $('body').attr('class');
    classTemplate = classTemplate.split('-');
    return classTemplate[1];
}

var bundleAjaxCart = (function (module, $) {
    'use strict';

    // Public functions
    var init, loadCart;

    // Private general variables
    var settings, isUpdating, $body;

    // Private plugin variables
    var $formContainer,
        $addToCart,
        $cartCountSelector,
        $cartCostSelector,
        $cartContainer;

    // Private functions
    var updateCountPrice,
        formOverride,
        itemAddedCallback,
        itemErrorCallback,
        cartUpdateCallback,
        buildCart,
        cartCallback,
        adjustCart,
        adjustCartCallback,
        qtySelectors,
        validateQty,
        bundleBuildCart;

    /*============================================================================
      Initialise the plugin and define global options
      ==============================================================================*/
    init = function (options) {
        // Default settings
        settings = {
            formSelector: 'form[action^="/cart/add"]',
            cartContainer: '#CartContainer',
            addToCartSelector: 'input[type="submit"]',
            cartCountSelector: '.cart-item-count, .cart-count',
            cartCostSelector: null,
            moneyFormat: '${{amount}}',
            disableAjaxCart: false,
            enableQtySelectors: true
        };

        // Override defaults with arguments
        $.extend(settings, options);

        // Select DOM elements
        $formContainer = $(settings.formSelector);
        $cartContainer = $(settings.cartContainer);
        $addToCart = $formContainer.find(settings.addToCartSelector);
        $cartCountSelector = $(settings.cartCountSelector);
        $cartCostSelector = $(settings.cartCostSelector);

        // General Selectors
        $body = $('body');

        // Track cart activity status
        isUpdating = false;

        // Setup ajax quantity selectors on the any template if enableQtySelectors is true
        if (settings.enableQtySelectors) {
            qtySelectors();
        }

        // Take over the add to cart form submit action if ajax enabled
        if (!settings.disableAjaxCart && $addToCart.length) {
            formOverride();
        }

        // Run this function in case we're using the quantity selector outside of the cart
        adjustCart();
    };

    loadCart = function () {
        $body.addClass('drawer--is-loading');
        ShopifyAPI.getCart(cartUpdateCallback);
    };

    updateCountPrice = function (cart) {
        if ($cartCountSelector) {
            $cartCountSelector.html(cart.item_count).removeClass('hidden-count');

            if (cart.item_count === 0) {
                $cartCountSelector.addClass('hidden-count');
            }
        }
        if ($cartCostSelector) {
            $cartCostSelector.html(
                theme.Currency.formatMoney(cart.total_price, settings.moneyFormat)
            );
        }
    };

    formOverride = function () {
        $formContainer.on('submit', function (evt) {
            evt.preventDefault();

            // Add class to be styled if desired
            $addToCart.removeClass('is-added').addClass('is-adding');

            // Remove any previous quantity errors
            $('.qty-error').remove();

            ShopifyAPI.addItemFromForm(
                evt.target,
                itemAddedCallback,
                itemErrorCallback
            );
        });
    };

    itemAddedCallback = function () {
        $addToCart.removeClass('is-adding').addClass('is-added');

        ShopifyAPI.getCart(cartUpdateCallback);
    };

    itemErrorCallback = function (XMLHttpRequest) {
        var data = eval('(' + XMLHttpRequest.responseText + ')');
        $addToCart.removeClass('is-adding is-added');

        if (data.message) {
            if (data.status === 422) {
                $formContainer.after(
                    '<div class="errors qty-error">' + data.description + '</div>'
                );
            }
        }
    };

    cartUpdateCallback = function (cart) {
        // Update quantity and price
        // updateCountPrice(cart);
        bundleBuildCart(cart);
    };

    bundleBuildCart = function (cart) {
        var items = [];

        if (!cart.items.length) {
            return buildCart(cart);
        }

        cart.items.forEach(function (e) {
            items.push({
                id: e.product_id,
                variant_id: e.id,
                variant_title: e.variant_title,
            });
        });

        var offers = bundleGetBundleTypeOffers(bundleJsonData, items);
        if (!offers.length) {
            return buildCart(cart);
        }

        bundleOfferRenderCartItem(cart, offers, function (cartItems) {
            if (!cartItems.length) {
                return buildCart(cart);
            }

            var noteCart = {};
            cartItems.forEach(function (e) {
                noteCart[e.variant_id] = e.quantity;
            });
            $.post('/cart/update.js', { updates: noteCart }).always(function (res) {
                var newCart = JSON.parse(res.responseText);
                updateCountPrice(newCart);
                buildCart(newCart);
            });
        });

    };

    buildCart = function (cart) {
        // Start with a fresh cart div
        $cartContainer.empty();

        // Show empty cart
        if (cart.item_count === 0) {
            $cartContainer.append(
                '<p class="cart--empty-message">' +
                theme.strings.cartEmpty +
                '</p>\n' +
                '<p class="cookie-message">' +
                theme.strings.cartCookies +
                '</p>'
            );
            cartCallback(cart);
            return;
        }

        // Handlebars.js cart layout
        var items = [],
            item = {},
            data = {},
            source = $('#CartTemplate').html(),
            template = Handlebars.compile(source);

        $.each(cart.items, function (index, cartItem) {
            var prodImg;
            if (cartItem.image !== null) {
                prodImg = cartItem.image
                    .replace(/(\.[^.]*)$/, '_small$1')
                    .replace('http:', '');
            } else {
                prodImg = '//cdn.shopify.com/s/assets/admin/no-image-medium-cc9732cb976dd349a0df1d39816fbcc7.gif';
            }

            if (cartItem.properties !== null) {
                $.each(cartItem.properties, function (key, value) {
                    if (key.charAt(0) === '_' || !value) {
                        delete cartItem.properties[key];
                    }
                });
            }

            var noQty = false, noRemoved = false;
            if (cartItem.variant_title && cartItem.variant_title.indexOf("Free") !== -1) {
                noQty = true;
                noRemoved = true;
            }

            item = {
                key: cartItem.key,
                line: index + 1,
                url: cartItem.url,
                img: prodImg,
                name: cartItem.product_title,
                variation: cartItem.variant_title,
                properties: cartItem.properties,
                itemAdd: cartItem.quantity + 1,
                itemMinus: cartItem.quantity - 1,
                itemQty: cartItem.quantity,
                price: theme.Currency.formatMoney(cartItem.price, settings.moneyFormat),
                discountedPrice: theme.Currency.formatMoney(
                    cartItem.price - cartItem.total_discount / cartItem.quantity,
                    settings.moneyFormat
                ),
                discounts: cartItem.discounts,
                discountsApplied:
                    cartItem.price === cartItem.price - cartItem.total_discount
                        ? false
                        : true,
                vendor: cartItem.vendor,
                variant_id: cartItem.variant_id,
                noQty: noQty,
                noRemoved: noRemoved,
            };

            items.push(item);

        });

        // Gather all cart data and add to DOM
        data = {
            items: items,
            note: cart.note,
            totalPrice: theme.Currency.formatMoney(
                cart.total_price,
                settings.moneyFormat
            ),
            totalCartDiscount:
                cart.total_discount === 0
                    ? 0
                    : theme.strings.cartSavings.replace(
                        '[savings]',
                        theme.Currency.formatMoney(
                            cart.total_discount,
                            settings.moneyFormat
                        )
                    )
        };

        $cartContainer.append(template(data));

        cartCallback(cart);
    };

    cartCallback = function (cart) {
        $body.removeClass('drawer--is-loading');
        $body.trigger('ajaxCart.afterCartLoad', cart);

        if (window.Shopify && Shopify.StorefrontExpressButtons) {
            Shopify.StorefrontExpressButtons.initialize();
        }
    };

    adjustCart = function () {
        // Delegate all events because elements reload with the cart

        // Add or remove from the quantity
        $body.on('click', '.ajaxcart__qty-adjust', function () {
            if (isUpdating) {
                return;
            }
            var $el = $(this),
                line = $el.data('line'),
                variant_id = $el.data('variant-id'),
                $qtySelector = $el.siblings('.ajaxcart__qty-num'),
                qty = parseInt($qtySelector.val().replace(/\D/g, ''));

            qty = validateQty(qty);

            // Add or subtract from the current quantity

            if ($el.hasClass('ajaxcart__qty--remove')) {
                qty = 0;
            } else {
                if ($el.hasClass('ajaxcart__qty--plus')) {
                    qty += 1;
                } else {
                    qty -= 1;
                    if (qty <= 0) qty = 0;
                }
            }



            // If it has a data-line, update the cart.
            // Otherwise, just update the input's number
            if (variant_id) {
                updateQuantity(variant_id, line, qty);
            } else {
                $qtySelector.val(qty);
            }
        });

        // Update quantity based on input on change
        $body.on('change', '.ajaxcart__qty-num', function () {
            if (isUpdating) {
                return;
            }
            var $el = $(this),
                line = $el.data('line'),
                variant_id = $el.data('variant-id'),
                qty = parseInt($el.val().replace(/\D/g, ''));

            qty = validateQty(qty);

            // If it has a data-line, update the cart
            if (variant_id) {
                updateQuantity(variant_id, line, qty);
            }
        });

        // Prevent cart from being submitted while quantities are changing
        $body.on('submit', 'form.ajaxcart', function (evt) {
            if (isUpdating) {
                evt.preventDefault();
            }
        });

        // Highlight the text when focused
        $body.on('focus', '.ajaxcart__qty-adjust', function () {
            var $el = $(this);
            setTimeout(function () {
                $el.select();
            }, 50);
        });

        function updateQuantity(variant_id, line, qty) {
            isUpdating = true;

            // Add activity classes when changing cart quantities
            var $row = $('.ajaxcart__row[data-line="' + line + '"]').addClass(
                'is-loading'
            );

            if (qty === 0) {
                $row.parent().addClass('is-removed');
            }

            // Slight delay to make sure removed animation is done
            setTimeout(function () {
                $.post('/cart/update.js', "updates[" + variant_id + "]=" + qty + "").always(function (res) {
                    adjustCartCallback(JSON.parse(res.responseText));
                });
                // ShopifyAPI.changeItem(line, qty, adjustCartCallback);
            }, 250);
        }

        // Save note anytime it's changed
        $body.on('change', 'textarea[name="note"]', function () {
            var newNote = $(this).val();

            // Update the cart note in case they don't click update/checkout
            ShopifyAPI.updateCartNote(newNote, function () { });
        });
    };

    adjustCartCallback = function (cart) {
        // Update quantity and price
        updateCountPrice(cart);

        // Reprint cart on short timeout so you don't see the content being removed
        setTimeout(function () {
            bundleBuildCart(cart);
            ShopifyAPI.getCart(bundleBuildCart);
            isUpdating = false;
        }, 500);
    };

    qtySelectors = function () {
        // Change number inputs to JS ones, similar to ajax cart but without API integration.
        // Make sure to add the existing name and id to the new input element
        var $numInputs = $('input[type="number"]');

        if ($numInputs.length) {
            $numInputs.each(function () {
                var $el = $(this),
                    currentQty = $el.val(),
                    inputName = $el.attr('name'),
                    inputId = $el.attr('id');

                var itemAdd = currentQty + 1,
                    itemMinus = currentQty - 1,
                    itemQty = currentQty;

                var source = $('#JsQty').html(),
                    template = Handlebars.compile(source),
                    data = {
                        key: $el.data('id'),
                        itemQty: itemQty,
                        itemAdd: itemAdd,
                        itemMinus: itemMinus,
                        inputName: inputName,
                        inputId: inputId
                    };

                // Append new quantity selector then remove original
                $el.after(template(data)).remove();
            });

            // Setup listeners to add/subtract from the input
            $('.js-qty__adjust').on('click', function () {
                var $el = $(this),
                    $qtySelector = $el.siblings('.js-qty__num'),
                    qty = parseInt($qtySelector.val().replace(/\D/g, ''));

                qty = validateQty(qty);

                // Add or subtract from the current quantity
                if ($el.hasClass('js-qty__adjust--plus')) {
                    qty += 1;
                } else {
                    qty -= 1;
                    if (qty <= 1) qty = 1;
                }

                // Update the input's number
                $qtySelector.val(qty);
            });
        }
    };

    validateQty = function (qty) {
        if (parseFloat(qty) === parseInt(qty) && !isNaN(qty)) {
            // We have a valid number!
        } else {
            // Not a number. Default to 1.
            qty = 1;
        }
        return qty;
    };

    module = {
        init: init,
        load: loadCart
    };

    return module;

})(ajaxCart || {}, jQuery);