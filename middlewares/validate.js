'use strict';

const { check, validationResult } = require('express-validator/check');

const checkShopDomain = check('shop').exists().isLength({ min: 13 }).custom((value, { req }) => isNaN(value));
const checkHmac = check('hmac').exists().isString();
const checkStatus = check('status').exists().isString();
const checkStatusInterger = check('status').exists().isNumeric();
const checkFirstName = check('firstName').exists().isString();
const checkLastName = check('lastName').exists().isString();
const checkEmail = check('email').exists().isEmail();
const checkPassword = check('password').exists().isString();
const checkNewPassword = check('newPassword').exists().isString();
const checkId = check('id').exists().isString();
const check_Id = check('_id').exists().isString();
const checkName = check('name').exists().isString();
const checkType = check('type').exists().isString();
const checkStyleURI = check('styleURI').exists().isString();
const checkProductId = check('product_id').exists();
const checkProducts = check('products').exists().isArray();

function requiredInstall(req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return next(errors.mapped());
    }
    next()
}

module.exports = {
    checkShopDomain, requiredInstall, checkStatus, checkStatusInterger, checkHmac,
    checkFirstName, checkLastName, checkEmail, checkPassword, checkNewPassword,
    checkId, check_Id, checkName, checkType, checkStyleURI, checkProductId, checkProducts,
}