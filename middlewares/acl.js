'use strict';

const _ = require('lodash');
const node_acl = require('acl');
const User = require('../models/user');

let acl = new node_acl(new node_acl.memoryBackend());

// This creates a set of roles which have permissions on
// different resources.

acl.allow('admin', ['/api/user', '/api/marketer', '/api/setting', '/api/kpi', '/api/product'], '*')
acl.allow('marketer', ['/api/campaign'], '*')

// Inherit roles
acl.addRoleParents('admin', 'marketer');
acl.addRoleParents('admin', 'guest');

User.find({}, (err, users) => {
    if (err) throw err;
    _.forEach(users, e => {
        acl.addUserRoles(String(e._id), String(e.role));
    })
});

module.exports = acl;