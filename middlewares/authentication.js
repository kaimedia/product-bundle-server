'use strict';

const _ = require('lodash');
const jwt = require('jsonwebtoken');
const config = require('../config');

function required(req, res, next) {

    var token = _.split(req.headers.authorization, " ", 2)[1];
    if (!token)
        return res.status(401).send({ success: false, message: 'Authentication is required.' });

    jwt.verify(token, config.SECRET, function (err, decoded) {
        if (err)
            return res.status(401).send({ success: false, message: 'Unauthorized' });

        req.userId = decoded.id;
        req.query.uid = req.userId;
        next();
    });

}

exports.required = required;