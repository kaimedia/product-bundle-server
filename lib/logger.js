'use strict';

const winston = require('winston');
const config = winston.config;
const moment = require('moment-timezone');
const timestamp = () => moment().tz('Asia/Ho_Chi_Minh').format('YYYY-MM-DD HH:mm:ss Z');

const logs = new (winston.Logger)({
    handleExceptions: false,
    transports: [
        new (winston.transports.Console)({
            level: 'info',
            colorize: true,
            json: false,
            timestamp: timestamp,
            formatter: function (options) {
                // - Return string will be passed to logger.
                // - Optionally, use options.colorize(options.level, <string>) to
                //   colorize output based on the log level.
                return '[' + options.timestamp() + '] ' +
                    config.colorize(options.level, options.level.toUpperCase()) + ' : ' +
                    (options.message ? options.message : '') +
                    (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '');
            }
        })
    ],
    exitOnError: false
});

module.exports = logs;